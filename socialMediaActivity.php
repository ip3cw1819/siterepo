<?php
include("header.php");
?>
    <div class="container-fluid">
        <div class="row justify-content-center">
            <div class="col-12">
                <div class="information pt-1 pl-4">
                    <h1>CryptoCompare Social Media Figures</h1>
                    <p>If you're interested in learning how this visualisation was created please see 
                    <a href="tutorialSocialMediaActivity.php"><font color="blue">here</a></p>
                </div>
                <div class="container" style="position: relative; height:75vh; width:80vw">
                    <canvas id="myChart"></canvas>
                </div>
            </div>
        </div>
    </div>
    <!--Calls scripts to be used in the chart creation-->
    <script src="https://cdn.jsdelivr.net/npm/chart.js@2.8.0"></script>
    <script src="https://cdn.jsdelivr.net/npm/roughjs@3.1.0/dist/rough.js"></script>
    <script src="https://cdn.jsdelivr.net/npm/chartjs-plugin-rough@0.2.0"></script>
    <script src="https://momentjs.com/downloads/moment.js"></script>
    <script>
        // Places chart colours into an array to be used during creation
        var chartColors = {
            green: '#136F63',
            yellow: '#E0CA3C',
            red: '#F34213',
            purple: '#3E2F5B',
            black: '#000F08'
        };
        // Sets the API URL
        var Query_URL = "https://min-api.cryptocompare.com/data/social/coin/histo/day?";
        // API Key needed to access data
        var Api_Key = "&api_key=86c8dd466a461331543dc4eeb88131f61e43a9d13515e3133fc8859e93487a6a";
        var chart_type = "horizontalBar";
        var label_description = "Active Users: ";
        var label = [];
        // Chart data is placed into these variables
        var chart_label = [];
        var DataLabel1 = [];
        var DataLabel2 = [];
        var DataLabel3 = [];
        var DataLabel4 = [];
        var UNIX_Timestamp = [];
        var dateTime = [];

        /* global $*/
        $.ajax({
            url: (Query_URL + Api_Key),
            type: "GET",
            dataType: "json",
            success: function (data) {
                console.log(data);
                for (var i = 0; i < data.Data.length; i++) {
                    DataLabel1.push(data.Data[i].reddit_comments_per_day);
                    DataLabel2.push(data.Data[i].reddit_posts_per_day);
                    DataLabel3.push(data.Data[i].reddit_comments_per_hour);
                    DataLabel4.push(data.Data[i].reddit_posts_per_hour);
                    chart_label.push(data.Data[i].reddit_active_users);
                    label.push(data.Data[i]);
                    UNIX_Timestamp.push(data.Data[i].time);
                }

                /*global moment*/
                for (var x = 0; x < UNIX_Timestamp.length; x++) {
                    dateTime.push(moment.unix(UNIX_Timestamp[x]).format("DD MMM YYYY"));
                }

                var ctx = document.getElementById("myChart").getContext('2d');
                new Chart(ctx, {
                    type: chart_type,
                    data: {
                        labels: dateTime,
                        datasets: [
                            { // Average Comments per hour Dataset
                                label: "Total Comments",
                                //Gives each bar a different color
                                backgroundColor: chartColors.green,
                                borderColor: chartColors.black,
                                borderWidth: 1,
                                //Data pulled 1from the api which is set inside of an array, and is displayed accordingly
                                data: DataLabel1,//End of Data

                            },//End of dataset
                            { // Average Posts per hour Dataset
                                label: "Total Posts",
                                //Gives each bar a different color
                                backgroundColor: chartColors.yellow,
                                borderColor: chartColors.black,
                                borderWidth: 1,

                                //Data pulled from the api which is set inside of an array, and is displayed accordingly
                                data: DataLabel2,//End of Data

                            },//End of dataset
                            { // Total Comments per day dataset
                                label: "Average Comments Per hour",
                                //Gives each bar a different color
                                backgroundColor: chartColors.red,
                                borderColor: chartColors.black,
                                borderWidth: 1,
                                //Data pulled 1from the api which is set inside of an array, and is displayed accordingly
                                data: DataLabel3
                                ,//End of Data

                            },//End of dataset
                            { // Total Posts per day Dataset
                                label: "Average Posts Per Hour",
                                //Gives each bar a different color
                                backgroundColor: chartColors.purple,
                                borderColor: chartColors.black,
                                borderWidth: 1,
                                //Data pulled from the api which is set inside of an array, and is displayed accordingly
                                data: DataLabel4,//End of Data

                            }//End of dataset
                        ]//End of datasets
                    },
                    options: {
                        layout: {
                            padding: {
                                left: 15,
                                right: 15
                            }
                        },
                        tooltips: {
                            mode: 'index'
                        },
                        responsive: true,
                        maintainAspectRatio: false,
                        legend: {
                            display: true
                        },
                        title: {
                            display: true,
                            fontColor: 'black',
                            text: "r/CryptoCurrency Daily Posts and Comments for last 30 days",
                        },
                        elements: {
                            rectangle: {
                                borderWidth: 2
                            }
                        },
                        scales: {
                            xAxes: [{
                                stacked: true,
                            }],
                            yAxes: [{
                                stacked: true,
                            }]
                        },
                    }
                });
                /*global Chart*/
                Chart.defaults.global.defaultFontFamily = '"Indie Flower", cursive';
                Chart.defaults.global.defaultFontSize = 14;


            }
        });
    </script>
<?php
include("eof.php");
?>