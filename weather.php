<?php
include("header.php");

?>

<div style="padding-left: 0;" class="container-fluid">
    <div class="row">
        <div class="col-sm-6">
            <div class="card">
                <h3 id="weatherheader" class="text-center">Weather Page</h3>
                <p class="text-center">Welcome to the Weather Page! Here you will be able to search the 5 Day Weather Forecast for any city,
                                  using the search functions below:</p>
            
                 <div class="input-group">
                  <label class = "input-group-append text-center" for="city">City Name: </label>
                  <input type="text" class="form-control col-6 mb-3" placeholder="Glasgow, Edinburgh etc..." value="" name="search" id="city">
                  <div class="input-group-append">
                    <button class="btn btn-outline-secondary mb-3" type="button" id="submit">Submit</button>
                  </div>
                </div>

                <div class="input-group">
                  <label class = "input-group-append text-center pr-2" for="lat">Latitude/Longitude: </label>
                  <input type="text" class="form-control d-block col-sm-2" value="" name="lat" id="lat">
                  <input type="text" class="form-control d-block col-sm-2 ml-1 " value="" name="lon" id="lon"> 
                  <div class="input-group-append">
                    <button class="btn btn-outline-secondary" type="button" id="submit1">Submit</button>
                  </div>
                </div>
              
            </div>

                <div class="card content1 ">
                <h3 class="card-title text-center mx-auto">Forecast (5 Days)</h3>
                <h5 id ="datey" class="card-title text-center mx-auto"></h5>
                <h6 id =time class="card-title text-center mx-auto"></h6>
                <div class="container">
                <div class="row justify-content-center">
                    <div id="monday" class="day col-sm-2 text-center verticalLine"></div>
                    <div id="tuesday"  class="day col-sm-2 text-center verticalLine day"><p></p></div>
                    <div id="wednesday"  class="day col-sm-2 text-center verticalLine"><p></p></div>
                    <div id="thursday" class="day col-sm-2  text-center  verticalLine"><p></p></div>
                    <div id="friday"  class="day col-sm-2 text-center  "><p></p></div>
 
                </div>
                </div>

            </div><!--End of Card-->
            

        </div><!--End Col 6-->
        
        <div class="col-sm-6">
            <div style="margin-top: 0;" class="card">
                <div class="card">
                <div style="height:40%;" class="" id="map"></div>
            </div>
            <h3 style="margin-top: 5px;" class="card-title text-center mx-auto">Today's Forecast</h3>
                <canvas id="graph"></canvas>
            </div>
        </div>
</div>
</div>

<script src ="/js/weather.js"></script>
<script async defer src="https://maps.googleapis.com/maps/api/js?key=AIzaSyDL7b5ov5dHqIZ90VYe9XL_O0GoD9FN0KE&callback=initMap"></script>
<?php
include("eof.php");
?>