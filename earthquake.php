<?php
include("header.php");
?>

        <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
        
<div class="container-fluid" style="padding-left: 0;">
    <div class="row">
        <div class ="col-12 pr-0">
            <div id="earthquakemap"></div>
        </div>
    </div>
    <div class="container">
    <div class="row justify-content-center">
        <div class="col-5 buttons-left float-left" ></div> 
        <div class="col-4 buttons-right float-right" ></div> 
        
        
        
    </div>
    </div>
</div>





<script>


        function initMap() {
            map = new google.maps.Map(document.getElementById('earthquakemap'), {
                zoom: 2,
                center: new google.maps.LatLng(55, -4), // Center Map. Set this to any location that you like
                mapTypeId: 'terrain' // can be any valid type
            });
        }
        
        
        
        //var thelocation;
        //var titleName;
        var button_text;
        var time_range = "hour";
        var mag = "significant_";
        var map;
        var markers = [];
        var markerCluster;




        //initMap() called when Google Maps API code is loaded - when web page is opened/refreshed 

    



    
$(document).ready(function() {
   
  for(var i = 0; i <=3; i++) {
      
                switch(i){
                    case 0: button_text = "Past 30 days";
                    break;
                    
                    case 1: button_text = "Past 7 days";
                    break;
                    
                    case 2: button_text = "Past day";
                    break;
                    
                    case 3: button_text = "Past hour";
                    break;
                }
                
     $('.buttons-left').append('<button type="button" id=b' + i +' data-role="button" class="btn btn-primary m-1">'+button_text+'</button>').trigger('create');
  }
  
  for(var y= 0; y <=4; y++) {
                //console.log(y);
                
            
                switch(y){
                    case 0: button_text = "1.0";
                    break;
                    
                    case 1: button_text = "2.5";
                    break;
                    
                    case 2: button_text = "4.5";
                    break;
                    
                    case 3: button_text = "Significant";
                    break;
                    
                    case 4: button_text = "All";
                    break;
                }
                
     $('.buttons-right').append('<button type="button" id=magb' + y + ' data-role="button" class="btn btn-primary m-1">'+button_text+'</button>').trigger('create');
  }
  
/*
      $('#b1').on("click",function() {
              console.log("fanny");  
            });
*/                                      
                            
                            

            $('#b0').on('click', function(){time_range ="month"; load_data();});
            $('#b1').on('click', function(){time_range ="week"; load_data();});
            $('#b2').on('click', function(){time_range ="day"; load_data();});
            $('#b3').on('click', function(){time_range ="hour"; load_data();});
                
            $('#magb0').on('click', function(){mag= "1.0_"; load_data();});
            $('#magb1').on('click', function(){mag ="2.5_"; load_data();});
            $('#magb2').on('click', function(){mag ="4.5_"; load_data();});
            $('#magb3').on('click', function(){mag ="significant_"; load_data(); });
            $('#magb4').on('click', function(){mag ="all_"; load_data(); }); 
            


});


    

            function load_data() {
                            
                            
                   if(markers.length > 0)
                    {
                    DeleteMarkers();
                    markerCluster.clearMarkers();
                                
                    }
                            
                            //DeleteMarkers();

                $.ajax({
                    // The URL of the specific data required

                    url:  "https://earthquake.usgs.gov/earthquakes/feed/v1.0/summary/" + mag + time_range + ".geojson",
                    
                    // Called if there is a problem loading the data
                    error: function () {
                        $('#info').html('<p>An error has occurred</p>');
                    },
                    // Called when the data has succesfully loaded
                    success: function (data) {
                        
                        console.log(data);
                        i = 0;
                        //markers = []; // keep an array of Google Maps markers, to be used by the Google Maps clusterer
             
                            
                        $.each(data.features, function (key, val) {
                            // Get the lat and lng data for use in the markers
                            var coords = val.geometry.coordinates;
                            var latLng = new google.maps.LatLng(coords[1], coords[0]);
                            var location = val.properties.place;
                            var mag = val.properties.mag;
                            var q_string = location.split(' ').join('+');
                            // Now create a new marker on the map
                            var marker = new google.maps.Marker({
                                position: latLng,
                                map: map,

                            
                                
                            });
                            
                            the_href = val.properties.url + "\'" + ' target=\'_blank\'';
                                
                            markers[i++] = marker; // Add the marker to array to be used by clusterer
                            
                        var infowindow = new google.maps.InfoWindow({
                                
                                  content: location + '.</br>Magnitude: ' + mag + '</br>' + '<a href=http://www.google.com/search?q=geographical+information+' + q_string + ">Find out more!</a>"
                        });
                        markers.push(marker);
                        marker.addListener('click', function(){
                            infowindow.open(map, marker);
                        })
                        });
                        
                         markerCluster = new MarkerClusterer(map, markers,
                            { imagePath: 'https://developers.google.com/maps/documentation/javascript/examples/markerclusterer/m' });


                            
                    }
                });
                };
                
        function DeleteMarkers() {
            for (var z = 0; z < markers.length; z++) {markers[z].setMap(null);} markers.length=0;
            /*
            while(markers.length){markers.pop().setMap(null);}
            markers.length=0;
            */
        };

    

    </script>

    <!-- Need the following code for clustering Google maps markers-->
    <script src="https://developers.google.com/maps/documentation/javascript/examples/markerclusterer/markerclusterer.js">
    </script>
    <!-- Need the following code for Google Maps -->
    <script async defer src="https://maps.googleapis.com/maps/api/js?key=AIzaSyBdlFKs_EEoJFBdt5w5_spCWDiqlseJlDw&callback=initMap">
    </script>
    

</body>

</html>