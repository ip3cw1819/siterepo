<?php

include("header.php");

?>

<div class="container-fluid" style="padding-left: 0;">
    <div class="row">
        <div class="col-12">
            <div class="container">
                <h2 class="text-center">Meteor Page</h2>
                <p>The Meteor Visualisation page takes free data provided by nasa to search their database on the meteors that have fallen over a number of years. The page allows the user to search by "year" and it will return 
                details on the meteors that have fallen in that year, and will place points on the google map where they have fallen. Please click on the links<strong>below</strong> to start the tutorial:</p>
                <div class="panel-group" id="accordion">
                    <div class="panel panel-default">
                        <div class="card">
                            <div class="panel-heading">
                                <h4 class="panel-title">
                                    <a data-toggle="collapse" data-parent="#accordion" href="#collapseget">1. Getting Started</a>
                                </h4>
                           </div>
                            <div id="collapseget" class="panel-collapse collapse in">
                                <div class="panel-body">
                                    <p>In order to pull the meteor data from an external API, the most fundamental part is that you need to have jQuery installed.
                                    The reasons we need jQuery is as followed:</p>
                                    <ul>
                                    <li>It gives us access to AJAX - Asyncronous JavaScript and XML - feature</li>
                                    <li>AJAX is the method of exchanging data with a server, and updating parts of a web page asyncronously - without reloading the whole page.</li>
                                    <li>When retrieving external data ideally we want to access the data live incase their are any changes made, and overall its more user friendly having all the requests being handled in the background, saving the refresh times.</li>
                                    <li>To access jQuery + Ajax the two most common methods are: </li>
                                    <ul>
                                        <li> 1.Placing a link to the external content delivery network (CDN) (<span class="code-snippet"><code>&lt;script integrity="sha256-fNXJFIlca05BIO2Y5zh1xrShK3ME+/lYZ0j+ChxX2DA="
                                              crossorigin="anonymous"src = "https://code.jquery.com/jquery-3.3.1.slim.js">&lt;/script></code></span>)</li>
                                        <li>  -This is known as the "quick" method, as there is no need to install the package, however it is unrealiable as its reliant on another website running to access.</li>
                                        <li> 2. The second most common method is to download jQuery itself from: "https://jquery.com/download/" and once thats done you will need to include the following tags to access jQuery
                                        <span class="code-snippet"><code>&lt;script src ='js/jquery-3.3.1.min.js'>&lt;/script></code></span></li>
                                        <li>3. Lastly, under the jQuery script the following script needs to be included in order for AJAX to run (<span class="code-snippet"><code>&lt;script integrity="sha384-b/U6ypiBEHpOf/4+1nzFpr53nxSS+GLCkfwBdFNTxtclqqenISfwAzpKaMNFNmj4"
                                              crossorigin="anonymous"src = "https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.11.0/umd/popper.min.js">&lt;/script></code></span>)</li>
                                    </ul>
                                    </ul> 
                                </div>
                            </div>
                        </div>
                    </div>
                    
                    <div class="panel panel-default">
                        <div class="card">
                            <div class="panel-heading">
                                <h4 class="panel-title">
                                    <a data-toggle="collapse" data-parent="#accordion" href="#collapse2">2. Ajax Requests/API</a>
                                </h4>
                            </div>
                            <div id="collapse2" class="panel-collapse collapse in">
                                <div class="panel-body">
                                    <p>Now that jQuery is included into the workspace, Ajax Requests can now be made to external servers, and in this example to the "NASA Meteor Data</p>
                                    <p>This API allows you to see all meteors that have dropped across the world. URL: "https://data.nasa.gov/Space-Science/Meteorite-Landings/gh4g-9sfhi"</p>
                                    <p>In order to gain access to their data you need to create a free account and obtain an API key.</p>
                                    <p>Inside their documentation, they have an in depth section outlining the many ways that you can filter and access their data.</p>
                                    <ul>
                                        
                                                <li>An Ajax Request take 3 main parameters:</li>
                                                <ul>
                                                    <li>1. URL - The address you wish to retrieve the data from.</li>
                                                    <li>2. Type - What type of request you want to make to the server which will either be POST or GET, in this case we want to use a GET request as we are recieving information</li>
                                                    <li>3. Success Function - What you want to happen when the data is successfully retrieved</li>
                                                </ul>
                                                
                                                <li>For an ajax request, you need to provide a URL to the api you wish to pull data from, and the url for the nasa
                                                         meteor API is:<br><code>url: ("https://data.nasa.gov/resource/y77d-th95.json?year=" + year + "-01-01T00:00:00.000"),</code></li>
                                                    <li>Furthermore, you will need an additonal parameter called data - which is to give your private api key 
                                                    and to limit the amount of results you wish to return which lools like the following:</li>
                            
                                                    <img class="image-responsive col-sm-6" src="img/meteor.JPG"/>
                                                                
                                                <li>To view what data is being returned from the api, inside the success function type the following: (<span class="code-snippet"><code>console.log(data);</code></span>)</li>
                                       </ul> 
                                   </div>
                            </div>
                        </div>
                    </div>
                    
                    <div class="panel panel-default">
                        <div class="card"> 
                            <div class="panel-heading">
                                <h4 class="panel-title">
                                    <a data-toggle="collapse" data-parent="#accordion" href="#collapse3">3. Google Maps API (Markers/Cluster/Infowindows)</a>
                                </h4>
                            </div>
                            <div id="collapse3" class="panel-collapse collapse in">
                                <div class="panel-body">
                                    <p>This is an optional task, but we can place the data we just pulled and place it on to google maps using the Google Maps API.</p>
                                    <ul>
                                    <li>To use the google maps api you need to do the following:</li>
                                    <ul>
                                        <li>1. Navigate to: https://developers.google.com/maps/documentation/javascript/get-api-key</li>
                                        <li>2. Follow their instructions to gain access to recieve an API key</li>
                                        <li>3. Once the sign up is complete, in your workspace you need to include the following script at the bottom of your page: <br> <code>&lt;script async defer src="https://maps.googleapis.com/maps/api/js?key=YOURAPICODE&callback=initMap" &lt;/script></code></span></li>
                                    </ul>
                                    
                                    <li>Next, the following function needs to be written so that the map has a callback(refrenced inside the script above):</li>
                                    
                                    <img class="image-responsive col-sm-6" src="img/maps.JPG" style=""/>
                                    
                                    <li>For an ajax request, you need to provide a URL to the api you wish to pull data from, and the url for the nasa
                                         meteor API is:<br>
                                    <code>url: ("https://data.nasa.gov/resource/y77d-th95.json?year=" + year + "-01-01T00:00:00.000"),</code></li>
                                    
                                    <li>Furthermore, you will need an additonal parameter called data - which is to give your private api key 
                                    and to limit the amount of results you wish to return which lools like the following:</li>
                                    <img class="image-responsive col-sm-6" src="img/meteor.JPG" style=""/>

                                    <li>To access the data from the API, we need to loop through the data brough back and push them
                                        into their correct arrays. Also, the loops serves the purpose of placing markers on the map, and adding 
                                        the google maps cluster - The number on a cluster indicates how many markers it contains.</li>
                                        
                                        
                                    <li>Each time the user clicks the submit button,
                                        a new request is sent to the server to get the data for a different yearr and data amount
                                        Meaning each time the this call is being made it is placing the new markers at the each of the coordinates
                                        where the meteors have landed. </li>
                                        
                                    
                                    <li>To present information inside an infowindow when the marker is clicked, we need to add the data inside of the
                                     content parameter inside of<code>var infoWindow</code>(see previous screenshot).
                                     Furthermore, we need to add an event listener, that listens for when a marker is clicked it will present the information 
                                     inside the corrosponding info window. The following code demonstrates all mentioned (All code must be inside the success function of 
                                     the Ajax Request)</li>
                                        
                                    <img class="image-responsive col-sm-6" src="img/meteorcode.JPG" style="" />
                                    
                                    <li>Lastly, include the following script at the bottom of your page: 
                                    <br><code>&lt;script src="https://developers.google.com/maps/documentation/javascript/examples/markerclusterer/markerclusterer.js"&lt;/script></code></li>
                                    
                                    <li>Finally, when you refresh your page your map should look similar (Search Dependant) to this:</li>
                                    <img class="image-responsive col-sm-6" src="img/clust.JPG" style=""/>
                                    </ul> 
                                </div>
                            </div>
                        </div>
                    </div>

                    
                    
                    <div class="panel panel-default">
                        <div class="card">
                            <div class="panel-heading">
                                <h4 class="panel-title">
                                    <a data-toggle="collapse" data-parent="#accordion" href="#collapse5">4. ChartJs</a>
                                </h4>
                            </div>
                            <div id="collapse5" class="panel-collapse collapse in">
                                <div class="panel-body">
                                    <p>Next, the data can be put into a chart using a jQuery library called ChartJs.
                                       If you have never used chartJS before I reccomend reading their documentation before
                                       undergoing this tutorial.</p>
                                    <ul>
                                    
                                    <li>Firstly, to use ChartJs on your project, you need to include the following CDN link to the top of your script:<br>
                                    <code>&lt;script src =https://cdnjs.cloudflare.com/ajax/libs/Chart.js/2.7.3/Chart.min.js'>&lt;/script></code></li>
                                    <li>To display a chartJS graph on your page you need to include the following tag with an id: <code>&lt;canvas id="graph"&lt;/canvas></code></li>
                                    <li>Next, in inside your success function, you have to create an instance of a chart and appoint it a 
                                    target ID, Type of graph, followed by the actual data and labels to form the graph. The code for that can be found below:</li><br>
                                    <img class="image-responsive col-sm-6" src="img/graphcode.JPG" style=""/>
                                    <li>We are using the data from the API, to compare which meteors had the biggest mass of that year.</li>
                                    <img class="image-responsive col-sm-6" src="img/meteorchart.JPG" style=""/>
                                    <li>Another feature of chartJs, is that its interactive and if you put your cursor over each plot of data,
                                    it will give additional info like so:</li>
                                     <img class="image-responsive col-sm-6" src="img/graphmeteor.JPG" style=""/>
                                    
                                    </ul> 
    
                                </div>
                            </div>
                        </div>
                    </div>
                    
                    
                    
                    <div class="panel panel-default">
                        <div class="card">
                            <div class="panel-heading">
                                <h4 class="panel-title">
                                    <a data-toggle="collapse" data-parent="#accordion" href="#collapse6">5. Searching by year</a>
                                </h4>
                            </div>
                            <div id="collapse6" class="panel-collapse collapse in">
                                <div class="panel-body">
                                    <p>Start by firstly opening the meteor page in another browser to follow along with this tutorial properly.</p>
                                    <ul>
                                    <li>Navigate to the input box at the top-left hand side of the screen which looks like:</li><img class="image-responsive col-sm-6" src="img/search.JPG" style=""/>
                                    <li>Next type in the year you want to return meteor fallage in, followed by the amount of results you wish to return.</li>
                                    <li>Once clicked it should return the following:</li></li><img class="image-responsive col-sm-6" src="img/full.JPG" style=""/>
                                    <li>As shown above, the markers and clusters are placed on the map by where they have fallen and if you click a map marker it brings up 
                                    the following information:
                                    </li>
                                    <img class="image-responsive col-sm-6" src="img/markmeteor.JPG" style=""/>
                                    
                                    <li>Lastly, a graph is shown underneath the map, that displays the variations in meteor masses amongst the results brought back.
                                    </li>
                                    </ul> 
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="panel panel-default">
                        <div class="card">
                            <div class="panel-heading">
                                <h4 class="panel-title">
                                    <a data-toggle="collapse" data-parent="#accordion" href="#collapse8">References</a>
                                </h4>
                            </div>
                            <div id="collapse8" class="panel-collapse collapse in">
                                <div class="panel-body">
                                    <ul>
                                    <li><a style="color:blue !important;" href ="https://data.nasa.gov/Space-Science/Meteorite-Landings/gh4g-9sfh">Nasa Weather Api</a></li>
                                    <li><a style="color:blue !important;" href="http://api.jquery.com/html/">jQuery</a></li>
                                    <li><a style="color:blue !important;" href="https://www.chartjs.org/">Chartjs</a></li>
                                    <li><a style="color:blue !important;" href="https://developers.google.com/maps/documentation/">Google Maps Api</a></li>
                                    </ul> 
    
                                </div>
                            </div>
                        </div>
                    </div>
            </div>
        </div>
    </div>
</div>

<?php
include("eof.php");
?>

                                