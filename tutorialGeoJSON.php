<!--https://tools.ietf.org/html/rfc7946#section-3.1-->
<!--This link contains the information used in the creation of the GeoJSON tutorial-->
<?php
include("header.php");
?>
<div class="container-fluid" style="padding-left: 0;">
    <div class="row">
        <div class="col-12">
            <div class=" pt-4 container">
                <h2>An Introduction to GeoJSON</h2>
                <p><strong>GeoJSON</strong> is an Open Standard Format which
                    was designed to be used when
                    representing Geographical features, along with their non-spatial attributes.
                    The Format is based on JSON (JavaScript Object Notation). The following tutorial uses data outlined
                    in the <a href="https://tools.ietf.org/html/rfc7946"><font color="blue">RFC</font></a>.</p>
                <div class="panel-group" id="accordion">
                    <!--Introduces the Concepts within GeoJSON-->
                    <div class="panel panel-default">
                        <div class="pl-4 card">
                            <div class="panel-heading">
                                <h4 class="panel-title">
                                    <a data-toggle="collapse" data-parent="#accordion"
                                       href="#collapse1">Introduction</a>
                                </h4>
                            </div>
                            <div id="collapse1" class="panel-collapse collapse in">
                                <div class="pb-2 panel-body"> GeoJSON is what is known as a "geospatial data
                                    interchange
                                    format" based on JSON. It helps us define several
                                    types of JSON objects in addition to the manner in which they can be combined to
                                    represent data about geographical features, their properties and
                                    and spatial extents they have.<br><br>
                                    All features within GeoJSON contain Geometry Object and additional properties,
                                    the FeatureCollection will contain a list of features used within GeoJSON.
                                    The information we'll be covering is not strictly new to GeoJSON,
                                    rather it was derived from existing Geographical Information standards the creators
                                    of GeoJSON streamlined to better suit a web development environment.
                                    Further in this tutorial we'll go over the different types of Geometric Objects
                                    you'll be
                                    working with when using GeoJSON. GeoJSON has grown massively in popularity since
                                    it's
                                    inception in the original 2008 publication and is most commonly
                                    used when working with web-mapping libraries, JSON-based document databases, and web
                                    APIs.
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="panel panel-default">
                        <div class="pl-4 card">
                            <div class="panel-heading">
                                <h4 class="panel-title">
                                    <a data-toggle="collapse" data-parent="#accordion" href="#collapse2">JSON</a>
                                </h4>
                            </div>
                            <div id="collapse2" class="panel-collapse collapse in">
                                <div class="pb-2 panel-body"> JSON that uses human-readable text to transmit data
                                    objects
                                    consisting of
                                    attribute–value pairs and array data types (or any other serializable value).
                                    It is a very common data format used for asynchronous browser–server
                                    communication,
                                    including as a replacement for XML in some AJAX-style systems.
                                </div>
                            </div>
                        </div>
                    </div>
                    <!--GeoMetric Objects Tutorial Information Accordian Start-->
                    <div class="panel panel-default">
                        <div class="card">
                            <div class="pl-4 panel-heading">
                                <h4 class="panel-title">
                                    <a data-toggle="collapse" data-parent="#accordion" href="#collapse3">Geometric
                                        Objects</a>
                                </h4>
                            </div>
                            <div id="collapse3" class="panel-collapse collapse">
                                <div class="pb-2 pl-4 panel-body">
                                    <div class="pl-4"> A Geometry object represents points, curves, and surfaces in
                                        coordinate space. Every Geometry object is a GeoJSON object no
                                        matter where it occurs in a GeoJSON text
                                    </div>
                                    <!--Beginning of Sub accordion for the GeoMetric Objects-->
                                    <div class="panel panel-default">
                                        <div>
                                            <div class="panel-heading">
                                                <h4 class="panel-title">
                                                    <a data-toggle="collapse" data-parent="#accordion"
                                                       href="#collapse3-1">Position</a>
                                                </h4>
                                            </div>
                                            <!--Information pertaining to Position obtained in https://tools.ietf.org/html/rfc7946#section-3.1-->
                                            <div id="collapse3-1" class="panel-collapse collapse">
                                                <div class="panel-body">The Position is the fundamental geometry
                                                    construct. These Coordinates are composed of either:
                                                    <ul class="pl-4 pr-4 list-group list-group-flush">
                                                        <li class="list-group-item">One position in the case of a Point
                                                            geometry,
                                                        </li>
                                                        <li class="list-group-item">An array of positions in the case of
                                                            a LineString or MultiPoint
                                                            geometry,
                                                        </li>
                                                        <li class="list-group-item">An array of LineString or linear
                                                            ring
                                                            coordinates in the case of a Polygon or MultiLineString
                                                            geometry,
                                                            or
                                                        </li>
                                                        <li class="list-group-item">An array of Polygon coordinates in
                                                            the case of a MultiPolygon
                                                            geometry.
                                                        </li>
                                                    </ul>
                                                    The position is just an array of numbers. You need to have at least
                                                    two of the above elements to get accurate coordinates,
                                                    the first two elements selected will function as the Longitude and
                                                    Latitute.
                                                    Additionally you can include the Altitude and Elevation as an
                                                    optional element.
                                                    <br>
                                                    It is recommended that you do not extend the positions before the
                                                    three elements specified as any
                                                    additional elements are ambigious and can lead to errors in the
                                                    output
                                                </div>
                                            </div>
                                        </div>
                                        <div>
                                            <div class="panel-heading">
                                                <h4 class="panel-title">
                                                    <a data-toggle="collapse" data-parent="#accordion"
                                                       href="#collapse3-2">Point</a>
                                                </h4>
                                            </div>
                                            <div id="collapse3-2" class="panel-collapse collapse">
                                                <div class="pb-2 panel-body"> The coordinates for the Point is a single
                                                    Position<br>
                                                    Example:<p class="pl-4"><font color="red">{ <br> "type": "Point",
                                                            <br>
                                                            "coordinates": [30, 10] <br>
                                                            } </font></p>
                                                </div>
                                            </div>
                                        </div>
                                        <div>
                                            <div class="panel-heading">
                                                <h4 class="panel-title">
                                                    <a data-toggle="collapse" data-parent="#accordion"
                                                       href="#collapse3-3">MultiPoint</a>
                                                </h4>
                                            </div>
                                            <div id="collapse3-3" class="panel-collapse collapse">
                                                <div class="pb-2 panel-body">The MultiPoint uses an array of positions
                                                    to create the coordinates member
                                                </div>
                                            </div>
                                        </div>
                                        <div>
                                            <div class="panel-heading">
                                                <h4 class="panel-title">
                                                    <a data-toggle="collapse" data-parent="#accordion"
                                                       href="#collapse3-4">LineString</a>
                                                </h4>
                                            </div>
                                            <div id="collapse3-4" class="panel-collapse collapse">
                                                <div class="pb-2 panel-body"> The LineString uses an array of two or
                                                    more positions for the coordinates member
                                                </div>
                                            </div>
                                        </div>
                                        <div>
                                            <div class="panel-heading">
                                                <h4 class="panel-title">
                                                    <a data-toggle="collapse" data-parent="#accordion"
                                                       href="#collapse3-5">MultiLineString</a>
                                                </h4>
                                            </div>
                                            <div id="collapse3-5" class="panel-collapse collapse">
                                                <div class="pb-2 panel-body"> The MultiLineString uses and array of
                                                    LineString coordinate arrays for the coordinates member
                                                </div>
                                            </div>
                                        </div>
                                        <div>
                                            <!--The Polygon Geometric Object-->
                                            <div class="panel-heading">
                                                <h4 class="panel-title">
                                                    <a data-toggle="collapse" data-parent="#accordion"
                                                       href="#collapse3-6">Polygon</a>
                                                </h4>
                                            </div>
                                            <div id="collapse3-6" class="panel-collapse collapse">
                                                <div class="pb-2 panel-body"> Polygons are usually somewhat more complex
                                                    than the other Geometric Objects we use and it should be noted that
                                                    within
                                                    the
                                                    <a href="https://tools.ietf.org/html/rfc7946#section-3.1.6"><font
                                                                color="blue">RFC</font></a> the coordinates member "must
                                                    be an array of linear ring of coordinate arrays".
                                                    For Polygons we need to specify a specific restraint, in which
                                                    Linear Rings come in very useful. <br>
                                                    The Linear Ring is simply a closed LineString containing four or
                                                    more positions but it must be noted that the first and last
                                                    positions are equivalent,
                                                    and must contain identical values in addition to being represented
                                                    in an identical fashion. It's worth noting that Polygons are not
                                                    explicitly represented as a GeoJSON geometry type,
                                                    but it's formulation leads to the following definition:<br><br>
                                                    <ul class="pl-4 pr-4 list-group list-group-flush">
                                                        <li class="list-group-item">The Coordinates must be an array of
                                                            linear ring coordinate arrays
                                                        </li>
                                                        <li class="list-group-item">If a polygon has more than one ring,
                                                            the first must be set as the exterior ring which bounds the
                                                            surface,
                                                            and the others are to be the interior rings that bouind
                                                            holes within the surface if they are present.
                                                        </li>
                                                    </ul>
                                                </div>
                                            </div>
                                        </div>
                                        <div>
                                            <div class="panel-heading">
                                                <h4 class="panel-title">
                                                    <a data-toggle="collapse" data-parent="#accordion"
                                                       href="#collapse3-7">MultiPolygon</a>
                                                </h4>
                                            </div>
                                            <div id="collapse3-7" class="panel-collapse collapse">
                                                <div class="pb-2 panel-body"> The MultiPolygon uses an array of Polygon
                                                    Coordinate arrays.
                                                </div>
                                            </div>
                                        </div>
                                        <div>
                                            <div class="panel-heading">
                                                <h4 class="panel-title">
                                                    <a data-toggle="collapse" data-parent="#accordion"
                                                       href="#collapse3-8">GeometryCollection</a>
                                                </h4>
                                            </div>
                                            <div id="collapse3-8" class="panel-collapse collapse">
                                                <div class="pb-2 pl-2 panel-body"> GeometryCollections contain a member
                                                    called "geometries", this member contains an array and each
                                                    element inside is considered a Geomtry Object. When working with a
                                                    GeometryCollection it's important to note that it lacks traditional
                                                    coordinates, instead of has geometries as mentioned before and it is
                                                    possible for it to be empty.
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="panel panel-default">
                            <div class="pl-4 card">
                                <div class="panel-heading">
                                    <h4 class="panel-title">
                                        <a data-toggle="collapse" data-parent="#accordion" href="#collapse4">Feature
                                            Objects</a>
                                    </h4>
                                </div>
                                <div id="collapse4" class="panel-collapse collapse">
                                    <div class="pb-2 panel-body"> As shown in the last section we're able to define
                                        "shapes" that we can then plot on a map.
                                        But it's important for these shapes to have some real world value to them, which
                                        is defined by the attributes of that shape. The name of
                                        an important landmark could be the attribute, in addition to additional
                                        information describing the object that has been plotted. When working with
                                        GeoJSON we use the Feature object to define both the attribute and geometric
                                        shape of a specific place.<br>
                                        The FeatureObject contains a member that has the name "type" with the value of
                                        "feature". The object will then also contain an object that has the name of
                                        "geometry" that contains the value of any of the above geometric shapes or a
                                        default null value. in addition, there is a member with a name of "properties"
                                        which has the value of the JSON object defining the attributes for the object.
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="panel panel-default">
                            <div class="pl-4 pb-1 card">
                                <div class="panel-heading">
                                    <h4 class="panel-title">
                                        <a data-toggle="collapse" data-parent="#accordion" href="#collapse5">FeatureCollection
                                            Object</a>
                                    </h4>
                                </div>
                                <div id="collapse5" class="panel-collapse collapse">
                                    <div class="pb-2 panel-body"> A GeoJSON object with a type of "FeatureCollection" is
                                        considered a FeatureCollection object. This object is the
                                        most common of the top-level contstructs that we will generally observe within a
                                        GeoJSON file. The FeatureCollection
                                        contains a member the name "features". The value of which is a JSON array. Each
                                        of the elements of this array is a feature object and it is
                                        possible for this array to be empty of values.
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="panel panel-default">
                            <div class="pl-4 pb-1 card">
                                <div class="panel-heading">
                                    <h4 class="panel-title">
                                        <a data-toggle="collapse" data-parent="#accordion" href="#collapse6">Bounding
                                            Box</a>
                                    </h4>
                                </div>
                                <div id="collapse6" class="panel-collapse collapse">
                                    <div class="pb-2 panel-body"> It's possible for a GeoJSON object to have a member
                                        named "bbox" that included information on the
                                        coordinate ranges for the Geometries, Features, or FeatureCollections, although
                                        it is not strictly required. As shown in the
                                        <a href="https://tools.ietf.org/html/rfc7946#section-5"><font
                                                    color="blue">RFC</font></a>
                                        This "bbox" member must be an array of "length 2*n where n is the number of
                                        dimensions represented in the
                                        contained geometries, with all axes of the most southwesterly point
                                        followed by all axes of the more northeasterly point. The axes order
                                        of a bbox follows the axes order of geometries".
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- You guys need to include EOF so that jquery and bootstrap js works -->
<?php
include("eof.php");
?>