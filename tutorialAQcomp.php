<?php

include("header.php");

?>

<div class="container-fluid" style="padding-left: 0;">
    <div class="row">
        <div class="col-12">
            <div class="container">
                <h2 class="text-center">Air quality visualisation tutorial</h2>
                <p>The air quality comparison page draws data from the OpenAQ api which delivers numerous atmospheric readings of different types of pollutants all over the world.
                This specific page allows the user to select their desired country and shows the levels of pollutants in bar chart form. Each half of the page co0ntains an area
                to display a chart allowing the user to compare two countries side by side and easily see the differences in air quality. More detailed information about the page will be included <strong>below.</strong></p>
                <div class="panel-group" id="accordion">
                    <div class="panel panel-default">
                        <div class="card">
                            <div class="panel-heading">
                                <h4 class="panel-title">
                                    <a data-toggle="collapse" data-parent="#accordion" href="#collapseget">1. Initial page load</a>
                                </h4>
                           </div>
                            <div id="collapseget" class="panel-collapse collapse in">
                                <div class="panel-body">
                                    <p>In order to allow the user to select which countries data to view, the first thing the page does on load is populate the two dropdown boxes
                                    , which take their contents from the API. This means that only countries that will have readings will be able to be chosen. Accomplishing this is a simple case 
                                    of retrieving the countires from the api and appending them to the dropdown box. The steps are as follows:</p>
                                    <ul>
                                    <li>Insert each dropdown box into a variable. (var dropdown = $("#CountryDrop");)</li>
                                    <li>Start an AJAX request that will return the countries supported by the API.</li>
                                    <li>Create an a loop that will run through each entry in the array and take the country name.</li>
                                    <ul style="list-style:none;">
                                        <li>$.each(data.results, function (val, text){</li>
                                        <li>$('#CountryDrop').append('&ltoption value="'+ data.results[val].code +'">' + data.results[val].name +'&lt/option>');</li>
                                        <li>}</li>
                                    </ul>
                                    </ul>There are a few variables that are created on page load as well, so that the chart functionality will work when a selection is made.</p>
                                        <ul>
                                            <li>var option - This stores the choice that is made in the dropdown box so that the necessary information can be pulled from the API.</li>
                                            <li>var ??level - This series of variables is used to store the levels of specific pollutant types for each country so that it can be placed into the array.
                                            These variables are overwritten each time a new choice is made.</li>
                                            <li>var check & var checkr - These two booleans record whether a chart already exists before trying to create a new one. When a chart is made it is set to one, then when a new choice is made the original chart is destroyed before creating the new one. This is necessary to prevent artifacts of the original graph appearing on the new one. This will be explored further in the chartjs heading.</li>
                                            <li>var left_choice & var right_choice - Used to store the map and its properties to display on the screen. </li>
                                            <li>var dropdown & var dropdownR - Used to store an array of google map markers so that they may be grouped by the clusterer.</li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                    </div>
                    
                    <div class="panel panel-default">
                        <div class="card">
                            <div class="panel-heading">
                                <h4 class="panel-title">
                                    <a data-toggle="collapse" data-parent="#accordion" href="#collapse2">2. API call</a>
                                </h4>
                            </div>
                            <div id="collapse2" class="panel-collapse collapse in">
                                <div class="panel-body">
                                    <p>For this page it was necessary to do several AJAX requests to retrieve the individual recordings for different pollutant types. This is due
                                    to the fact that countries have multiple recordings attached to them and it would be impossible to know beforehand which location in the array 
                                    the required entry is situated.</p>
                                    <p>The URL used in each request is formatted to only return a specific pollutant type as well as choose the measurement from location 0 in the array, which 
                                    is the most recent reading for the country.</p>
                                    <p>This measurement is then set to a variable to be used in the chart function to display the data.</p>
                                    <ul style="list-style:none;">
                                        <li>From the AQComp page - </li>
                                                       <li>$.ajax({</li>

                                                        <li>url: "https://api.openaq.org/v1/latest?country=" + option + "&ampparameter=pm25",</li>
                                                        <li>error: function () {</li>
                                                        <li>$('#info').html('&ltp>An error has occurred&lt/p>');</li>
                                                        <li>},</li>
                                                        <li>success: function (data) {</li>
                                                        <li>try{</li>
                                                        <li>pm25level = data.results[0].measurements[0].value;</li>
                                                        <li>}</li>
                                                        <li>catch{</li>
                                                        <li>pm25level = "0";</li>
                                                        <li>}</li>
                                        </ul> 
                                        
                                                <p>This accomplishes several things that are vital to the functionality of the page. In the url there are two parameters specified.
                                                The first is the country, this is taken from the choice in the dropdown box and is placed to filter any results that are not associated with 
                                                the country. The second is the pollutant type parameter. This filters out the other measurements made for that country and only
                                                returns one type. This is to make sure the correct data is placed into the variable to be used in the chart.</p>
                                                <p>The try catch is in place as some measurements for a country are not available. When the request tries to return a result where there is 
                                                no data an error occurs that prevents any action being taken by the rest of the function. By placing the variable in the try catch
                                                it will attempt to get data, but if there is no data it will set the variable to 0 so that the chart will show no reading.</p>
                                    
                                </div>
                            </div>
                        </div>
                    </div>
                    
                    <div class="panel panel-default">
                        <div class="card">
                            <div class="panel-heading">
                                <h4 class="panel-title">
                                    <a data-toggle="collapse" data-parent="#accordion" href="#collapse3">3. Chartjs</a>
                                </h4>
                            </div>
                            <div id="collapse3" class="panel-collapse collapse in">
                                <div class="panel-body">
                                    <p>Now that all necessary variables have been set to a value, the chart is ready to be created. Charts on this page are accomplished through use of the Chartjs
                                    library. Two canvas' are set on the page and styled so that they sit next to each other in the centre of the page, below their respective dropdown box. 
                                    Canvas' are essential for using Chartjs, as they are set as the foundation for the chart. </br> 
                                    The first step is to set the canvas as the "context" of the chart. This is telling the page where to draw the chart and its data. </br>
                                    A variable is then created to allow the chart object to be modified. A new chart object is created setting the canvas as its location and the required data as its contents. <br>
                                    If another selection is made the original chart has to be deleted. If it is not there will be data artifacts that will appear when the graph 
                                    is moused over. This is because the orginal graph is still on the page, but is just located behind the new graph.This is obviously not desirable as there is data that appears 
                                    when the mouse is hovered over the graph. </p>
                                    
                                    <ul style="list-style:none;">
                                        <li>From the AQComp page - </li>
                                                       <li>$('#CountryDropR').change(function(){</li>

                                                        <li>option = $(this).val();</li>
                                                        <li>if(checkr == 1){</li>
                                                        <li>window.myChartR.destroy();</li>
                                                        <li>checkr = 0;</li>
                                                        <li>right_choice = 1;</li>
                                                        <li>dataload();</li>
                                                        <li>}</li>
                                                        <li>else{</li>
                                                        <li>right_choice = 1;</li>
                                                        <li>dataload();</li>
                                                        <li>}</li>
                                                        <li>});</li>
                                        </ul> 
                                        
                                    <p>What this accomplishes is to first get the value of the country from the dropdown box. It will then run a check to see if there is already a chart existing
                                    on the relevant section using the variable made at the beginning of the page.</br> If a chart exists, it is destroyed, completely removing it from the page 
                                    allowing a clean slate for the new chart to be created. When the initial chart is made, or a new one after a chart is destroyed the dataload() function is called 
                                    which grabs the data from the api and calls the function to create the chart either in the left canvas or the right canvas depending on which dropdown box the 
                                    selection was made in. </p>
                                </div>
                            </div>
                        </div>
                    </div>                    
                    
                    
                    <div class="panel panel-default">
                        <div class="card">
                            <div class="panel-heading">
                                <h4 class="panel-title">
                                    <a data-toggle="collapse" data-parent="#accordion" href="#collapse4">References</a>
                                </h4>
                            </div>
                            <div id="collapse4" class="panel-collapse collapse in">
                                <div class="panel-body">
                                    <p>The following websites were used in the construction of this page:</p>
                                    <ul>
                                    <li>https://www.chartjs.org/</li>
                                    <li>https://openaq.org/#/?_k=0iktm9</li>
                                    <li>https://jquery.com/</li>
                                    <li >https://getbootstrap.com/</li>
                                    </ul> 
    
                                </div>
                            </div>
                        </div>
                    </div>
                    


            </div>

        </div>
    </div>
</div>

<?php
include("eof.php");
?>