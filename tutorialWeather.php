<?php

include("header.php");

?>

<div class="container-fluid" style="padding-left: 0;">
    <div class="row">
        <div class="col-12">
            <div class="container">
                <h2 class="text-center">Weather Page Introduction</h2>
                <p>The Weather Page is a place where you can receive various weather data from an open API, based on their city location or by Latitude and Longitude.
                The forecast returned will show you both the current forecast for today, and over the next 5 days, showing the fluctuation in temperatures through-out the day via
                a graph. Please click on the links <strong>below</strong> to start the tutorial:</p>
                <div class="panel-group" id="accordion">
                    <div class="panel panel-default">
                        <div class="card">
                            <div class="panel-heading">
                                <h4 class="panel-title">
                                    <a data-toggle="collapse" data-parent="#accordion" href="#collapseget">1. Getting Started</a>
                                </h4>
                           </div>
                            <div id="collapseget" class="panel-collapse collapse in">
                                <div class="panel-body">
                                    <p>In order to pull the weather data from an external API, the most fundamental part is that you need to have jQuery installed.
                                    The reasons we need jQuery is as follows:</p>
                                    <ul>
                                    <li>It gives us access to AJAX - Asyncronous JavaScript and XML - feature</li>
                                    <li>AJAX is the method of exchanging data with a server, and updating parts of a web page asyncronously - without reloading the whole page.</li>
                                    <li>When retrieving external data ideally we want to access the data live in case there are any changes made, and overall it's more user friendly having all the requests being handled in the background, saving the refresh times.</li>
                                    <li>To access jQuery + Ajax the two most common methods are: </li>
                                    <ul>
                                        <li> 1.Placing a link to the external content delivery network (CDN) (<span class="code-snippet"><code>&lt;script integrity="sha256-fNXJFIlca05BIO2Y5zh1xrShK3ME+/lYZ0j+ChxX2DA="
                                              crossorigin="anonymous"src = "https://code.jquery.com/jquery-3.3.1.slim.js">&lt;/script></code></span>)</li>
                                        <li>  -This is known as the "quick" method, as there is no need to install the package, however it is unrealiable as its reliant on another website running to access.</li>
                                        <li> 2. The second most common method is to download jQuery itself from: "https://jquery.com/download/" and once thats done you will need to include the following tags to access jQuery
                                        <span class="code-snippet"><code>&lt;script src ='js/jquery-3.3.1.min.js'>&lt;/script></code></span></li>
                                        <li>3. Lastly, under the jQuery script the following script needs to be included in order for AJAX to run (<span class="code-snippet"><code>&lt;script integrity="sha384-b/U6ypiBEHpOf/4+1nzFpr53nxSS+GLCkfwBdFNTxtclqqenISfwAzpKaMNFNmj4"
                                              crossorigin="anonymous"src = "https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.11.0/umd/popper.min.js">&lt;/script></code></span>)</li>
                                    </ul>
                                    </ul> 
                                </div>
                            </div>
                        </div>
                    </div>
                    
                    <div class="panel panel-default">
                        <div class="card">
                            <div class="panel-heading">
                                <h4 class="panel-title">
                                    <a data-toggle="collapse" data-parent="#accordion" href="#collapse2">2. Ajax Requests/API</a>
                                </h4>
                            </div>
                            <div id="collapse2" class="panel-collapse collapse in">
                                <div class="panel-body">
                                    <p>Now that jQuery is included into the workspace, Ajax Requests can now be made to external servers, and in this example to the "Open-Weather Map API</p>
                                    <p>Open Weather Map API is a free weather forecast API, that gives live weather updates for various cities across the world. URL: "https://openweathermap.org/api"</p>
                                    <p>In order to gain access to their data you need to create a free account and obtain an API key via email.</p>
                                    <p>Inside their documentation, they have various different types of data you can access, but for the purpose of this tutorial, I am going to be using the 5 day Forecast to match the weather page.</p>
                                    <ul>
                                        <li>This is an AJAX Request to the API:<br> <span class="code-snippet"><code>&lt;script>&lt;
                                        $.ajax ({
                                            url: ("https://api.openweathermap.org/data/2.5/forecast?q=(insert city name)&units=metric&mode=JSON&APPID=YOURAPICODE"),
                                          type: "GET",
                                            dataType: "json",
                                            success: function(data){ 
                                            }}   ); &lt;/script></code></span></li>
                                            
                                            <ul>
                                                <li>An Ajax Request take 3 main parameters:</li>
                                                
                                                <ul>
                                                    <li>1. URL - The address you wish to retrieve the data from.</li>
                                                    <li>2. Type - What type of request you want to make to the server which will either be POST or GET, in this case we want to use a GET request as we are receiving information</li>
                                                    <li>3. Success Function - What you want to happen when the data is successfully retrieved</li>
                                                </ul>
                                                
                                                <li>When making a request to the Open Weather Map API, the URL takes in a city name, what metric system you want, what format you want the data to be returned in, and finally your private API key.</li>
                                                
                                                <li>To view what data is being returned from the API, inside the success function type the following: (<span class="code-snippet"><code>console.log(data);</code></span>)</li>
                                                
                                                <img class="image-responsive col-sm-6" src="img/weathercode.JPG" style=""/>
                                                
                                                <li>As shown it brings back over 200 results for the 5 day forecast, that can be accessed by looping through the data and pushing the data into arrays:</li>
                                                
                                                <img class="image-responsive col-sm-6" src="img/code.JPG" style=""/>
                                                
                                                <li>Once the data is pushed into arrays we can display it how we like.</li>
                                            </ul>
                                       </ul> 
                                   </div>
                            </div>
                        </div>
                    </div>
                    
                    <div class="panel panel-default">
                        <div class="card"> 
                            <div class="panel-heading">
                                <h4 class="panel-title">
                                    <a data-toggle="collapse" data-parent="#accordion" href="#collapse3">3. Google Maps API (Markers/Infowindows)</a>
                                </h4>
                            </div>
                            <div id="collapse3" class="panel-collapse collapse in">
                                <div class="panel-body">
                                    <p>This is an optional task, but we can place the data we just pulled and place it on to Google Maps using the Google Maps API.</p>
                                    <ul>
                                    <li>To use the Google Maps API you need to do the following:</li>
                                    <ul>
                                        <li>1. Navigate to: https://developers.google.com/maps/documentation/javascript/get-api-key</li>
                                        <li>2. Follow their instructions to gain access to recieve an API key</li>
                                        <li>3. Once the sign up is complete, in your workspace you need to include the following script at the bottom of your page: <br> <code>&lt;script async defer src="https://maps.googleapis.com/maps/api/js?key=YOURAPICODE&callback=initMap" &lt;/script></code></span></li>
                                    </ul>
                                    
                                    <li>Next, the following function needs to be written so that the map has a callback(referenced inside the script above):</li>
                                    
                                    <img class="image-responsive col-sm-6" src="img/maps.JPG" style=""/>
                                    
                                    <li>To have the map visble on your page you will need to add a canvas tag with an id that matches to the map variable (Don't worry about the lat and lon search that will be explained yesterday:<br>
                                    <code>url: ("https://api.openweathermap.org/data/2.5/forecast?q=" + search +"&lat=" + latsearch +"&lon="+ lonsearch +"&units=metric&mode=JSON&APPID=YOURAPICODE"),</code></li>
                                    <li>Next, inside the for loop we previously created:</li>
                                    
                                    <img class="image-responsive col-sm-6" src="img/marks.JPG" style=""/>
                                    
                                    <li>Each time the user clicks the search button,
                                        a new request is sent to the server to get the data for a different location.
                                        Meaning each time the this call is being made it is placing a new marker at the coordinates of the new city.</li>
                                        
                                    <img class="image-responsive col-sm-6" src="img/map_1.JPG" style="" />
                                    
                                    <li>To present information inside an infowindow when the marker is clicked we have added a variable called(see previous screenshot)<code>var infoWindow</code>
                                        and an event listener, that listens for when a marker is clicked. Inside the variable it takes a parameter called content, which is 
                                        instantiated to a variable "contentConst1".</li>
                                        
                                    <img class="image-responsive col-sm-6" src="img/conten.JPG" style="" />
                                    
                                    <li>To display this information inside of the marker info window we need to set the info window's content parameter to: <code>contentCost1</code> :</li>
                                    <img class="image-responsive col-sm-6" src="img/marks.JPG" style=""/>
                                    
                                    <li>Next, refresh your page and search for a city, it should look similar to this (depending on your own personal styling).</li>
                                    
                                    <img class="image-responsive col-sm-6" src="img/ax.JPG" style=""/>
                                    
                                        
                                    </ul> 
    
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="panel panel-default">
                        <div class="card">
                            <div class="panel-heading">
                                <h4 class="panel-title">
                                    <a data-toggle="collapse" data-parent="#accordion" href="#collapse4">4. 5 Day Forecast</a>
                                </h4>
                            </div>
                            <div id="collapse4" class="panel-collapse collapse in">
                                <div class="panel-body">
                                    <p>Another interesting thing you can do with the data is provide a forecast for the next 5 days for the users using some clever styling and jQuery:</p>
                                    <ul>
                                        <img class="image-responsive col-sm-6" src="img/5day.JPG" style=""/>
                                        <li>Using the data pulled from the API, and jQuery you will be able to select the divs and sections of your page you wish to display the data,
                                        but before we do this we need to loop the pulled data using a for loop that grabs the data at the index + 8 positions at each iteration, in order to grab
                                        the data for each different day at the same time, as the API doesn't provide a better way to do this.</li>
                                        <img class="image-responsive col-sm-6" src="img/8day.JPG" style=""/>
                                        <li>As shown above, at each iteration, the index is being increased by 8 to grab the forecast for each 5 days, meaning that it grabs data from postitions:
                                        "0, 8, 16, 24, 32" in the old arrays, and place them into new positions: "0, 1, 2, 3, 4" in the new array.</li>
                                        <li>To display this data using jQuery can be shown below: </li>
                                        <img class="image-responsive col-sm-6" src="img/fos.JPG" style=""/>               
                                        <li>jQuerys <code>$('#id').html();</code> This function gets all of the HTML content - in our case the weather data, and places it 
                                        inside the element that has been selected. in this example if you open a string and begin typing HTML it will automatically convert it to 
                                        HTML using this function. Furthermore, JavaScript variables and objects can be appended by closing the string and using the <code>+</code> operator
                                        and providing the array name with the index we want to take data from.</li>
                                    </ul> 
    
                                </div>
                            </div>
                        </div>
                    </div>
                    
                    
                    <div class="panel panel-default">
                        <div class="card">
                            <div class="panel-heading">
                                <h4 class="panel-title">
                                    <a data-toggle="collapse" data-parent="#accordion" href="#collapse5">5. ChartJs</a>
                                </h4>
                            </div>
                            <div id="collapse5" class="panel-collapse collapse in">
                                <div class="panel-body">
                                    <p>Next, the data can be put into a chart using a jQuery library called Chart.js.
                                       If you have never used Chart.js before I recommend reading their documentation before
                                       undergoing this tutorial.</p>
                                    <ul>
                                    
                                    <li>Firstly, to use Chart.js on your project, you need to include the following CDN link to the top of your script:<br>
                                    <code>&lt;script src =https://cdnjs.cloudflare.com/ajax/libs/Chart.js/2.7.3/Chart.min.js'>&lt;/script></code></li>
                                    <li>To display a Chart.js graph on your page you need to include the following tag with an id: <code>&lt;canvas id="graph"&lt;/canvas></code></li>
                                    <li>Next, inside your success function, you have to create an instance of a chart and appoint it a 
                                    target ID, Type of graph, followed by the actual data and labels to form the graph. The code for that can be found below:</li><br>
                                    <img class="image-responsive col-sm-6" src="img/graphcode.JPG" style=""/>
                                    <li>We are using the data from the API to fill the chart with the temperature at each hour over the day.
                                    Furthermore, the labels are being set from the time that is being pulled from the API which is relative to the current hour
                                    of the day. For example, if it is 5pm it will pull from 6PM onwards.</li>
                                    <img class="image-responsive col-sm-6" src="img/graph.JPG" style=""/>
                                    <li>Another feature of Chart.js, is that it's interactive and if you put your cursor over each plot of data,
                                    it will give additional info like so:</li>
                                     <img class="image-responsive col-sm-6" src="img/graphpic.png" style=""/>
                                    
                                    </ul> 
    
                                </div>
                            </div>
                        </div>
                    </div>
                    
                    
                    
                    <div class="panel panel-default">
                        <div class="card">
                            <div class="panel-heading">
                                <h4 class="panel-title">
                                    <a data-toggle="collapse" data-parent="#accordion" href="#collapse6">6. Searching by City</a>
                                </h4>
                            </div>
                            <div id="collapse6" class="panel-collapse collapse in">
                                <div class="panel-body">
                                    <p>Start by firstly opening the weather page in another browser to follow along with this tutorial properly.</p>
                                    <ul>
                                    <li>Navigate to the input box at the top-left hand side of the screen which looks like:</li><img class="image-responsive col-sm-6" src="img/windo.JPG" style=""/>
                                    <li>Next type in the city you want to return the weather forecast for by typing it inside of the input box, then pressing submit when completed.</li>
                                    <li>Once clicked it should return the following:</li></li><img class="image-responsive col-sm-6" src="img/Wheat.JPG" style=""/>
                                    <li>As shown above, a 5 day weather forecast is displayed under, where it displays a description of the weather for the day, an image according to the conditions,
                                    the highest/lowest temperatures and followed by the windspeed.
                                    </li>
                                    <li >On the right side of the screen, a marker is placed on the map, where if you click it does the following...
                                    </li>
                                    
                                    <li >Lastly, a graph is shown underneath the map, that displays the variation in temperatures at different hours throughout the day.
                                    </li>
                                    </ul> 
                                </div>
                            </div>
                        </div>
                    </div>
                    
                    <div class="panel panel-default">
                        <div class="card">
                            <div class="panel-heading">
                                <h4 class="panel-title">
                                    <a data-toggle="collapse" data-parent="#accordion" href="#collapse7">7. Searching by Latitude/Longitude.</a>
                                </h4>
                            </div>
                            <div id="collapse7" class="panel-collapse collapse in">
                                <div class="panel-body">
                                    <p>Start by firstly opening the weather page in another browser to follow along with this tutorial properly.</p>
                                    <ul>
                                    <li>Navigate to the input box at the top-left hand side of the screen which looks like:</li><img class="image-responsive col-sm-6" src="img/windo.JPG" style="display: inline-block;"/>
                                    <li>Next enter the Latitude/Longitude of the area you want to return the weather forecast for by typing it inside of the input button, then pressing submit when completed.</li>
                                    <li>Once clicked it should return the following:</li></li><img class="image-responsive col-sm-6" src="img/Wheat.JPG" style=""/>
                                    <li>As shown above, a 5 day weather forecast is displayed under, where it displays a description of the weather for the day, an image according to the conditions,
                                    the highest/lowest temperatures and followed by the windspeed.
                                    </li>
                                    <li >On the right side of the screen, a marker is placed on the map, where if you click it does the following...
                                    </li>
                                    
                                    <li >Lastly, a graph is shown underneath the map, that displays the variation in temperatures at different hours throughout the day.
                                    </li>
                                    </ul> 
    
                                </div>
                            </div>
                        </div>
                    </div>
                    
                    
                    
                    <div class="panel panel-default">
                        <div class="card">
                            <div class="panel-heading">
                                <h4 class="panel-title">
                                    <a data-toggle="collapse" data-parent="#accordion" href="#collapse8">References</a>
                                </h4>
                            </div>
                            <div id="collapse8" class="panel-collapse collapse in">
                                <div class="panel-body">
                                    <ul>
                                    <li><a style="color:blue !important;" href ="https://openweathermap.org/api">Open Weather Map API</a></li>
                                    <li><a style="color:blue !important;" href="http://api.jquery.com/html/">jQuery</a></li>
                                    <li><a style="color:blue !important;" href="https://www.chartjs.org/">Chart.js</a></li>
                                    <li><a style="color:blue !important;" href="https://developers.google.com/maps/documentation/">Google Maps API</a></li>
                                    </ul> 
    
                                </div>
                            </div>
                        </div>
                    </div>
            </div>
        </div>
    </div>
</div>

<?php
include("eof.php");
?>

                                