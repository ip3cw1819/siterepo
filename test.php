<?php
include("header.php");
?>




<div class="conatiner-fluid">
  <div class="row justify-content-center">
    <div class="col-sm-6">
      <div class="card">
        <h3 id="weatherheader" class="text-center">Weather Page</h3>
        <p class="text-center">
        Welcome to the Weather Page! Here you will be able to search the 5 Day Weather Forecast for any city, using the search functions below:</p>
        
        
        <div class="input-group">
          
          <label class = "input-group-append text-center" for="city">City Name: </label>
          <input type="text" class="form-control col-8 col-md-8 col-sm-4 mx-auto" placeholder="Glasgow, Edinburgh, Aberdeen, Airdrie etc..." value="" name="search" id="city"> 
          <div class="input-group-append">
            <button class="btn btn-outline-secondary" type="button" id="submit">Submit</button>
          </div><!--End of append-->
        </div><!--End of input group-->
        
        <div class="input-group">
            <label class = "input-group-append text-center" for="lat">Latitude/Longitude: </label>
            <input type="text" class="form-control d-block col-sm-2" value="" name="lat" id="lat">
            <input type="text" class="form-control d-block col-sm-2 " value="" name="lon" id="lon"> 
            <div class="input-group-append">
              <button class="btn btn-outline-secondary" type="button" id="submit1">Submit</button>
            </div>
        </div>
      </div><!-- End of card-->
    </div><!--End of col-8-->
    
    <!--Map-->
    <div class="col-sm-6">
          <div style="" class="card map-responsive">
                <div style="" class="" id="map"></div>
            </div>
    </div>
    
    
  </div><!-- End of row-->
</div><!--End of container-->


<script src ="/js/weather.js"></script>
<script async defer src="https://maps.googleapis.com/maps/api/js?key=AIzaSyDL7b5ov5dHqIZ90VYe9XL_O0GoD9FN0KE&callback=initMap"></script>
<?php
include("eof.php");
?>