<?php

include("header.php");

?>

<div class="container-fluid" style="padding-left: 0;">
    <div class="row">
        <div class="col-12">
            <div class="container">
                <h2 class="text-center">Earthquake Page Tutorial</h2>
                <p>Earthquakes are any seismic activity that causes shakes in the ground. This can be from activity in the tectonic plates of the earth, or by some 
                force acting on the earth, such as a sufficiently large explosion.This page draws from the United states Geological survey to display 
                the locations and magnitudes of Earthquakes around the world. This can then be filtered down to several periods of times and magnitudes to show a subset of the data. These time periods are the past hour, past day, past 7 days
                and the past 30 days. Within these time periods the severoty of the earthquake can be filtered by selecting one of the magnitude buttons, limiting it to
                all earthquakes, significant earthquakes or earthquakes above 1.0, 2.5 or 4.5 magnitude. More detailed information about the page will be included <strong>below.</strong></p>
                
                <div class="panel-group" id="accordion">
                    <div class="panel panel-default">
                        <div class="card">
                            <div class="panel-heading">
                                <h4 class="panel-title">
                                    <a data-toggle="collapse" data-parent="#accordion" href="#collapseget">1. Page setup</a>
                                </h4>
                           </div>
                            <div id="collapseget" class="panel-collapse collapse in">
                                <div class="panel-body">
                                    <p>Each section of this tutorial page will run through the important steps and processes occurring on the page from the moment it is loaded.
                                    Once a user is directed to the page, several things happen automatically to set up the system for use.</p>
                                    <ul>
                                    <li>The necessary CDN links are called to give us the tools necessary for the page</li>
                                    <li>The page is divided into specific areas to appropriately set out the different elements on the page, such as the map and the buttons. </li>
                                    <li>A google map object is set on one of the areas created and its properties are assigned. </li>
                                    <li>A loop is then started that will add the buttons to the page and apply their attributes.</li>
                                    <li>These buttons are then also applied to a seperate container so that they will not interfere with the map area. This also allows them to be seperately styled.</li>
                                    <li>A set of variables is has been created to be used in the functions of the page:</li>
                                        <ul>
                                            <li>var button_text - This is created to store the text to be used on the buttons at the bottom of the page. 
                                            As the for loop runs a switch statement is used to choose which text to include on the button.
                                            This is done twice. Once for the time period buttons and once again for the magnitude buttons.</li>
                                            <li>var time_range - This is used to store the required time period for use in the API query string. This is set when a button is pressed. Depending on which button is clicked changes the stored variable.</li>
                                            <li>var mag - Similar to the timerange variable, this stores the required magnitude for the API query string. It is also set when the button is pressed.</li>
                                            <li>var map - Used to store the map and its properties to display on the screen. </li>
                                            <li>var markers[] - Used to store an array of google map markers so that they may be grouped by the clusterer.</li>
                                            <li>var markerCluster - Used to allow the created marker clusters to be cleared. This prevents the map becoming too crowded by previous markers.</li>
                                            <li>var coords - Used to store the provided coordinates of an earthquake to apply a marker to the map.</li>
                                            <li>var latLng - used to apply the coords to a google maps function.</li>
                                            <li>var location - Used to store the location of the earthquake for the infowindow.</li>
                                            <li>var q_string - takes the data stored in location and replaces the spaces with a plus sign to conform to a google search format.</li>
                                            <li>var marker - used to store the properties of markers to be added onto the map.</li>
                                            <li>var infowindow - used to add an info window to each marker that supplies its location, magnitude and a link to further information on the geogrphical area.</li>
                                    </ul>
                                    </ul> 
                                </div>
                            </div>
                        </div>
                    </div>
                    
                    <div class="panel panel-default">
                        <div class="card">
                            <div class="panel-heading">
                                <h4 class="panel-title">
                                    <a data-toggle="collapse" data-parent="#accordion" href="#collapse2">2. AJAX Request</a>
                                </h4>
                            </div>
                            <div id="collapse2" class="panel-collapse collapse in">
                                <div class="panel-body">
                                    <p>Once the page has everything in place, it is ready to recieve and display the data from the API.</p>
                                    <p>This is accomplished through an AJAX request which will send a request to the API link and retrieve the data it has stored.</p>
                                    <p>Normally, this would return in the JSON format, but this page uses a geoJSON format to better suit the contents of the data.</p>
                                    <ul style="list-style:none;">
                                        <li>From the Earthquake page - </li>
                                                       <li>$.ajax({</li>

                                                        <li>url:  "https://earthquake.usgs.gov/earthquakes/feed/v1.0/summary/" + mag + time_range + ".geojson",</li>
                                                        <li>error: function () {</li>
                                                        <li>$('#info').html('<p>An error has occurred</p>');</li>
                                                        <li>},</li>
                                                        <li>success: function (data) {</li>
                                        </ul> 
                                        
                                                <p>There is one major parameter that an AJAX request requires to work, which is the url that it will send the request to.</br>
                                                    In this instance the beginning of the URL remains the same regardless of which parameters it is being filtered to.</br>
                                                    We will always want the data to be returned in a geojson format as well, so it is specified at the end of the url.
                                                    The variables in place are taken from the button selection, and filter the parameter selection down the required results.</p>
                                    
                                </div>
                            </div>
                        </div>
                    </div>
                    
                    <div class="panel panel-default">
                        <div class="card">
                            <div class="panel-heading">
                                <h4 class="panel-title">
                                    <a data-toggle="collapse" data-parent="#accordion" href="#collapse3">3. Button Use</a>
                                </h4>
                            </div>
                            <div id="collapse3" class="panel-collapse collapse in">
                                <div class="panel-body">
                                    <p>Once the map has been created, before the markers are created the page needs to know what time range and magnitude you are wishing to view.
                                    To accomplish this, the buttons that were created on page load provide the required information. When created time_range is set to hour and magnitude
                                    is set to significant_. This is so that when the first choice is made it will either be the most recent data available or only the most powerful
                                    earthquakes. </br></br> Once a new selection is made, the query variables are altered and the map repopulated with fresh markers. </p>
    
                                </div>
                            </div>
                        </div>
                    </div>                    
                    
                    <div class="panel panel-default">
                        <div class="card"> 
                            <div class="panel-heading">
                                <h4 class="panel-title">
                                    <a data-toggle="collapse" data-parent="#accordion" href="#collapse4">4. Markers and Clustering</a>
                                </h4>
                            </div>
                            <div id="collapse4" class="panel-collapse collapse in">
                                <div class="panel-body">
                                    <p>For the earthquake page, to show exact locations markers are used that take the latitude and longitude and place a precise marker where the event occurred. 
                                    If there are enough markers situated close together, they will be stored in the array and only displayed when the map is zoomed in enough to easily 
                                    distinguish them. Before this occurs they are replaced by a cluster marker that notifies the user how many markers are in that general vicinity.</p>
                                    <ul>
                                    <li>There are several steps to applying a marker to the map:</li>
                                    <ul>
                                        <li>1. Saving the coordinates from the retrieved data.</li>
                                        <li>2. Use the google.maps.LatLng() placing the coordinates in as arguments.</li>
                                        <li>3. Set a marker to a new google maps marker with the coordinates as its position and the map as its target.</li>
                                        <li>4. Once the marker has been created, add a listener that will react to a mouseclick and display the infowindow. </li>
                                    </ul>
                                    </ul>
                                    <p>This will only display the individual markers, there are some additional steps to include the clustering.</p>
                                    <ul>
                                    <li>1. Once the marker has been given its properties, you must add it to the markers array.</li>
                                    <li>2. Using the push function allows the marker to be added to the end of the array.</li>
                                    <li>3. This array can then be added to the markerClusterer object to group the markers automatically.</li>
                                    </ul> 
                                    <p>Once the markers are placed on the map, adding more markers will not remove the original set. This can lead to the map becoming incredibly cluttered
                                    and unreadable. To solve this issue, before markers are added the original set must be cleared.</br> There are a few steps to this process:</p>
                                    <ul style="list-style:none;">
                                        <li>From the Earthquake page -  </li>
                                                       <li>function DeleteMarkers(){</li>

                                                        <li>for (var z = 0; z < markers.length; z++) {markers[z].setmap(null);} markers.length=0;</li>
                                                        <li>};</li>
                                        </ul> 
                                        <p>This will set all markers to no value and clear the array allowing fresh markers in their place. To remove the clusters, further code is required:</p>
                                        <ul style="list-style:none;">                
                                                        <li>if(markers.length > 0)</li>
                                                        <li>}</li>
                                                        <li>DeleteMarkers();</li>
                                                        <li>markerCluster.clearMarkers();</li>
                                                        <li>}</li>
                                        </ul>
                                        <p>By first removing all instances of the markers, and then clearing the clusters, the map is free to be populated by the new set of markers.</p>

    
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="panel panel-default">
                        <div class="card">
                            <div class="panel-heading">
                                <h4 class="panel-title">
                                    <a data-toggle="collapse" data-parent="#accordion" href="#collapse5">5. External Linking</a>
                                </h4>
                            </div>
                            <div id="collapse5" class="panel-collapse collapse in">
                                <div class="panel-body">
                                    <p>One of the aspects of the infowindow is the final line that sends the user to a google search for the specified geographical location.
                                    This is accomplished by inputting the link tag in the content of the info window and setting
                                    the q_string to the end of it so that the location of the earthquake along with the words
                                    geographical information are searched when the link is clicked. The user is then free to discover more
                                    about the earthquake and area at their leisure.</p>
    
                                </div>
                            </div>
                        </div>
                    </div>
                    
                    

                    
                    <div class="panel panel-default">
                        <div class="card">
                            <div class="panel-heading">
                                <h4 class="panel-title">
                                    <a data-toggle="collapse" data-parent="#accordion" href="#collapse6">References</a>
                                </h4>
                            </div>
                            <div id="collapse6" class="panel-collapse collapse in">
                                <div class="panel-body">
                                    <p>The following websites were used in the construction of this page:</p>
                                    <ul>
                                    <li>https://earthquake.usgs.gov/fdsnws/event/1/</li>
                                    <li>https://developers.google.com/maps/documentation/</li>
                                    <li>https://jquery.com/</li>
                                    <li >https://getbootstrap.com/</li>
                                    </ul> 
    
                                </div>
                            </div>
                        </div>
                    </div>
                    


            </div>

        </div>
    </div>
</div>

<?php
include("eof.php");
?>

                                