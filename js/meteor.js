//var myChart;
var amount ="";
var del = [];
var map;
var markers = [];
var markers1 = []; 
var check = 1;
var markerCluster;
var year;
var mass;
var status;
var name;
var years;
var graph_mass=[];
var graph_names=[];
            
var reclat;
var reclong;
var years;
var geolocation;
var recclass;
var found;


$('#submit').on('click',function(){
        amount = $('#amount').val();
        year = $('#year').val();

        if(amount == "" ||amount > 400){
            alert("The search is limited to 400 results");
        }else if(year > 2013){
            $('.error').html('Please enter a valid year.');
        }
        else if(year <= 0){
            $('.error').html('Please enter a valid year.');
        }
        else if(year < 1890){
            $('.error').html('Please enter a valid year.');
        }

        else{

            yearSearch();

        }

});


function DeleteMarkers() {
     for (var z = 0; z < markers.length; z++)
     {
         markers[z].setMap(null);
     }
            
      markers.length=0; //markerCluster.clearItems();
            
};
        
    
function yearSearch(){
    if(markers.length > 0)
       {
         DeleteMarkers();
         markerCluster.clearMarkers();
       }

     $.ajax
    ({
        //sets url as worldbank API for education
        url: "https://data.nasa.gov/resource/y77d-th95.json?year="+year+"-01-01T00:00:00.000",
        //Using get to retrieve the information
        type: "GET",
        
        data: {
          "$limit": amount,
          "$$app_token" : "QRRl8ciRbKvgClMTzQItpIz5J",
        },
        //Formatting the data so it is returned in JSON format
        dataType: "json",
        //If the data is successfully pulled
        success: function(data)
        {
            console.log(data);
            console.log(data);
            
            $.each(data, function(key, val){
                 var coords = val.geolocation.coordinates;
                 var latLng = new google.maps.LatLng(coords[1], coords[0]);
                 
                name = val.name;
                mass = val.mass;
                reclat = val.reclat;
                reclong = val.reclong;
                found = val.fall;
                years = val.year;
                var marker = new google.maps.Marker({
                    
                    position: latLng,
                    map: map,
                });
                
                
                 var infoWindow = new google.maps.InfoWindow({
                    content: "<h4 class='text-center'>Meteor: " + name + "</h4> " +
                             "<p class='text-center'>Discovery: " + found + "</p> " +
                             "<p>Mass: " + mass + "</p>" +
                             "<p>Reclat: " + reclat + "</p>" +
                             "<p>Reclong: " + reclong + "</p>" +
                             "<p> Year: "+ years
                });
                
                 marker.addListener("click",function(){
            
                    infoWindow.open(map,marker);

                })
                

                markers[key] = marker;
                markers.push(marker);

            }); //End of $.each loop

            
              markerCluster = new MarkerClusterer(map, markers,
            { imagePath: 'https://developers.google.com/maps/documentation/javascript/examples/markerclusterer/m' });
            
             //myChart.destroy();
             
             graph_mass=[];
             graph_names=[];
             var searched = year;
             
             graph_mass.length = 0;
             graph_names.length = 0;
             
             for(var y = 0; y <data.length; y++){
                
                graph_mass.push(data[y].mass);
                graph_names.push(data[y].name);
             }

             
             if(window.myChart != null){
             window.myChart.destroy();
             var ctx = document.getElementById('graph');
            
             window.myChart = new Chart(ctx, {

                //Setting the chart to type "bar"
                type: 'bar',
                data: {
                  //Setting the chart labels to the date of enrollment each year I.E 2015-2019
                  labels: [graph_names[0], graph_names[1], graph_names[2],
                           graph_names[3], graph_names[4], graph_names[5],
                           graph_names[6], graph_names[7], graph_names[8],
                           graph_names[9], graph_names[10], graph_names[11],
                           graph_names[12], graph_names[13], graph_names[14],
                           graph_names[15],
                                            
                           ],
                  datasets: [
                    {
                      //Showing that each year is measured by %
                      label: "Mass",
                      
                      //Gives each bar a different color
                     backgroundColor: ["#3e95cd", "#8e5ea2","#3cba9f","#e8c3b9","#c45850","#c4c4c4", "#020811", "#a01415", "#835401", "#01c6b9", "#e309bf"],
                      
                      //Data pulled from the api which is set inside of an array, and is displayed accordingly
                      data: [graph_mass[0],graph_mass[1],
                             graph_mass[2],graph_mass[3],
                             graph_mass[4],graph_mass[5],
                             graph_mass[6],graph_mass[7],
                             graph_mass[8],graph_mass[9],
                             graph_mass[10],graph_mass[11],
                             graph_mass[12],graph_mass[13],
                             graph_mass[14],graph_mass[15],
                             
                            ],//End of Data
                            
                         
                    borderColor: '#0000FF',
                    
                             
                    }//End of dataset
                    
                  ]//End of dataset
                  
                },
                
                options: {
                  legend: { display: true },
                  title: {
                    display: true,
                    //Sets the title of the graph to the name pulled from the API
                    text: "Meteor's in the year: " + year,
                  }
                }
            });
            }
            else{
            var ctx = document.getElementById('graph');
            //window.myChart.destroy();
            
             window.myChart = new Chart(ctx, {

                //Setting the chart to type "bar"
                type: 'bar',
                data: {
                  //Setting the chart labels to the date of enrollment each year I.E 2015-2019
                  labels: [graph_names[0], graph_names[1], graph_names[2],
                           graph_names[3], graph_names[4], graph_names[5],
                           graph_names[6], graph_names[7], graph_names[8],
                           graph_names[9], graph_names[10],
                                            
                           ],
                  datasets: [
                    {
                      //Showing that each year is measured by %
                      label: "Mass",
                      
                      //Gives each bar a different color
                     backgroundColor: ["#3e95cd", "#8e5ea2","#3cba9f","#e8c3b9","#c45850","#c4c4c4", "#020811", "#a01415", "#835401", "#01c6b9", "#e309bf"],
                      
                      //Data pulled from the api which is set inside of an array, and is displayed accordingly
                      data: [graph_mass[0],graph_mass[1],
                             graph_mass[2],graph_mass[3],
                             graph_mass[4],graph_mass[5],
                             graph_mass[6],graph_mass[7],
                             graph_mass[8],graph_mass[9],
                             graph_mass[10]
                             
                            ],//End of Data
                            
                         
                    borderColor: '#0000FF',
                    
                             
                    }//End of dataset
                    
                  ]//End of dataset
                  
                },
                
                options: {
                  legend: { display: true },
                  title: {
                    display: true,
                    //Sets the title of the graph to the name pulled from the API
                    text: "Meteor's in the year: " + year,
                  }
                }
            });
        }

            }//End of Success function
    }); // Finish Ajax Function
}


function loadGraph(){
    
}
    
function initMap(){
    //clearMarkers();
    var options = 
    {
        zoom: 2,
        center:{lat: 55.8642, lng:-4.2518} 
    }
    map = new google.maps.Map(document.getElementById('map'), options);

        
}
