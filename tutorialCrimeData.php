<?php
include("header.php");
?>

<div class="container">
    <div class="row">
        <div class="col-12">
            <div class="page-header">
                <h1>Crime data visualisation tutorial <small>How the visualisation was created</small></h1>
            </div>
            <br>
            This page is designed to give you an idea as to how the <a class = "tutorial-link" href="dataVis1.php">crime data visualisation page</a> was created.
            It begins by instructing you how to access the D3.js library, moves on to the API where the data is held and how to access it before discussing how the visualisation itself is made.
            To discover more about a topic, please click or tap on any of the headings below.
            <br>
            <br>
        </div>
    </div>
    <div class="panel-group" id="accordion">
        <div class="row">
            <div class="col-12">
                <div class="panel panel-default">
                    <div class="card">
                        <div class="panel-heading">
                            <h4 class="panel-title">
                                <a data-toggle="collapse" data-parent="#accordion" href="#collapse1">1. D3.js Library</a>
                            </h4>
                        </div>
                        <div id="collapse1" class="panel-collapse collapse in">
                            <div class="panel-body">
                                <p>When it comes to actually creating the graph, access to the D3.js Library is required. D3.js (Data Driven Documents) is a JavaScript library used to create data visualisations in conjunction with HTML, CSS & SVG.
                                    In order to access the D3.js Library, a script tag will be needed within the head of the HTML document:
                                </p>
                                <code class="code-snippet">&lt;script src = "https://d3js.org/d3.v5.min.js">&lt;/script></code><br><br>
                                <p> OR </p>
                                <code class="code-snippet">&lt;script src = "js/d3.v5.min.js">&lt;/script></code>
                                <br><br>
                                <p>
                                    The above examples uses the most currently up to date version of D3.js. The "src" attribute needs the URL of the CDN (Content Delivery Network) or the location of the locally held file as shown in the examples above. The various versions of the D3.js library - and their respective ZIP folders - can be found within the "Releases" section of it's <a class="tutorial-link" href="https://github.com/d3/d3/releases">GitHub page</a>. The particular version of D3.js you use is very important if you are ever working from <a class= "tutorial-link" href = "https://github.com/d3/d3/wiki/Gallery" >previous examples</a> of D3.js visualisations. This is due to some versions of D3.js containing syntax that others will not and can result in errors being generated. The crime data visualisation uses version 3 of D3.js.
                                </p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-12">
                <div class="panel panel-default">
                    <div class="card">
                        <div class="panel-heading">
                            <h4 class="panel-title">
                                <a data-toggle="collapse" data-parent="#accordion" href="#collapse2">2. Crime data explorer API</a>
                            </h4>
                        </div>
                        <div id="collapse2" class="panel-collapse collapse in">
                            <div class="panel-body">
                                <p>
                                    The API (Application Programming Interface) the visualisation uses is the <a class="tutorial-link" href="https://crime-data-explorer.fr.cloud.gov/api">FBI's Crime Data Explorer API</a>. Before you are allowed to get any data back from the API, you must first <a class="tutorial-link" href="https://api.data.gov/signup/">sign up for an API key</a>. Once you have received your API key, you will be able to receive data back from any of the API endpoints available. The "endpoints" refer to various categories of data held by the API - some will hold data on offenders, some on victims and others relating to the different law enforcement agencies within the USA. Within each endpoint are various "operations" that the endpoint supports. These operations represent ways on how to return different kinds of data from the endpoint.
                                </p>
                                <p>
                                    Take the "victim-data-controller" endpoint for example; it contains two operations: one that returns victim data for a particular offense recorded by a <b>chosen agency</b> and the other will return the same data but collated nationally as opposed to just one agency.
                                </p>
                                <p>
                                    If this tutorial does not provide an adequate explanation on using the API then there is <a class="tutorial-link" href="https://github.com/fbi-cde/crime-data-frontend">documentation available</a> that may be of better use.
                                </p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-12">
                <div class="panel panel-default">
                    <div class="card">
                        <div class="panel-heading">
                            <h4 class="panel-title">
                                <a data-toggle="collapse" data-parent="#accordion" href="#collapse3">3. Viewing the data</a>
                            </h4>
                        </div>
                        <div id="collapse3" class="panel-collapse collapse">
                            <div class="panel-body">
                                <p>The visualisation uses the "victim-data-controller" endpoint and uses the parameters "homicide" and "age". Once you have entered these parameters and have clicked "Try it out!" you will find the "Response body" that has been generated returns an error. The error reads "No api_key was supplied. Get one at https://api.usa.gov:443". This is where your API key key comes in handy:
                                </p>
                                <ol>
                                    <li>First, copy the request URL that has been generated for you</li>
                                    <li>Paste it into the URL of a new tab</li>
                                    <li>Append the following to the URL: ?api_key=</li>
                                    <li>Add your API key to the end of the URL</li>
                                </ol>
                                <p>When you visit this address, you will find a large amount of JSON waiting for you - it is an array of JSON objects that each contain the number of homicides in the USA by age range for a given year.</p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-12">
                <div class="panel panel-default">
                    <div class="card">
                        <div class="panel-heading">
                            <h4 class="panel-title">
                                <a data-toggle="collapse" data-parent="#accordion" href="#collapse4">4. Storing and accessing the data</a>
                            </h4>
                        </div>
                        <div id="collapse4" class="panel-collapse collapse">
                            <div class="panel-body">
                                <p>
                                    When it comes to accessing remote sources of data (either data held on another site or data held locally), a very useful piece of code is an AJAX (Asynchronous JavaScript And XML) request. This tutorial will not go into depth on how to use AJAX but if you do want to read up on how to use it in conjunction with jQuery then please visit the <a class="tutorial-link" href="tutorialJavascript.php">JavaScript tutorial page</a>. Ideally, using AJAX, we should be able to use the following piece of code for accessing the JSON that the API provides. However if we use the following piece of code:
                                </p>
                                <code class="code-snippet">
                                $.ajax({<br>
                                "url": "https://api.usa.gov/crime/fbi/sapi/api/data/nibrs/homicide/victim/national/age?api_key=" + "" /*insert your API key here*/,<br>
                                "type": "get",<br>
                                "dataType": "json",<br>
                                "success": function(data) {<br>
                                console.log(data.results);<br>
                                },<br>
                                "error": function() {<br>
                                console.log("Error accessing data");
                                }<br>
                                })<br>
                                </code>
                                <p>
                                    Looking at the console within the browsers developer tools, the API has blocked the request and so to make a request to this URL, you will need to use CORS (Cross Origin Sharing Request). This tutorial will not cover CORS but if you wish to read further into it then please follow <a class="tutorial-link" href="https://developer.mozilla.org/en-US/docs/Web/HTTP/CORS">this link</a>.
                                </p>
                                <p>
                                    Instead of communicating with the URL, the crime visualisation simply uses a local file to function - the local file being the copied JSON from the URL we visited earlier. This method, while not ideal, serves our purposes as the data is updated annually and so will not go out of date any time soon. 
                                </p>
                                <p>
                                    Now take the above code snippet but replace the URL value with the location of the local JSON file. Within the console, you should be able to see the exact same data that we saw returned from the API and as a result, we are able to use the data to be displayed within the graph.
                                </p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-12">
                <div class="panel panel-default">
                    <div class="card">
                        <div class="panel-heading">
                            <h4 class="panel-title">
                                <a data-toggle="collapse" data-parent="#accordion" href="#collapse5">5. Creating the bar chart</a>
                            </h4>
                        </div>
                        <div id="collapse5" class="panel-collapse collapse">
                            <div class="panel-body">
                                <p>
                                    The graph that will be created in this section is based heavily off of an <a class="tutorial-link" href="http://bl.ocks.org/mstanaland/6100713">example</a> of a stacked bar chart. This example gives you the code necessary to create the bar chart but it will be up to you to replace the data being used with the data of your choosing. It should also be noted that it is beyond the scope of this section to give a line by line breakdown of creating the visualisation (over 200 lines of JavaScript have gone into creating the chart). Instead, this will be a somewhat broader overview of how the visualisation is being created and how the data we have retrieved works its way into it.
                                </p>
                                <!-- Section 5.1 -->
                                <div class="card">
                                    <div class="panel-heading">
                                        <h5 class="panel-title">
                                            <a data-toggle="collapse" data-parent="#accordion" href="#collapse5-1">5.1 Setting up the chart area</a>
                                        </h5>
                                    </div>
                                    <div id="collapse5-1" class="panel-collapse collapse">
                                        <div class="panel-body">
                                            <p>
                                                Before the any kind of visualisation can be displayed, there needs to be an area allocated within the HTML for it. The crime data visualisation uses an empty div with an ID of "chart" that the JavaScript can later hook on to.
                                            </p>
                                            <p>
                                                Moving on to the JavaScript, the entirety of the graph creation takes place within the success part of the AJAX request to the local JSON file. It begins by assigning values to two variables named "width" and "height". 
                                            </p>
                                            <ul>
                                                <li>Width is assigned the value of the window width (<code class="code-snippet">$(window).width()</code>) and subtracting 150 to allow for the legend and y-axis labelling to be displayed within the page without scrolling.
                                                </li>
                                                <li>
                                                    Height is assigned the value of the height of the body element (<code class="code-snippet">$(body).height()</code>) and subtracting the height of the h1 and button group elements.
                                                </li>
                                            </ul>
                                            Next, the code selects the div with the ID of "chart" and appends an SVG with the width and height attributes set to their respective variables that were just declared.
                                            The code then re-orders the JSON data so that it's arranged in chronological order (i.e. the earliest data_year to the most recent data_year). This is done so that the graph displays the bar chart with the bars in chronological order as well. The following code was used to do this:
                                            <code class="code-snippet">data.results.sort(function(a, b) {<br>
                                            return a.data_year - b.data_year;<br>
                                            });</code>
                                        </div>
                                    </div>
                                </div>
                                <!-- End of Section 5.1 -->
                                <!-- Section 5.2 -->
                                <div class="card">
                                    <div class="panel-heading">
                                        <h5 class="panel-title">
                                            <a data-toggle="collapse" data-parent="#accordion" href="#collapse5-2">5.2 The X and Y axes</a>
                                        </h5>
                                    </div>
                                    <div id="collapse5-2" class="panel-collapse collapse">
                                        <div class="panel-body">
                                            <p>
                                                Next, the code creates the x and y axes for the bar chart. It figures out the x-axis by looking at the number of data sets it will be using and using a set of chained D3 functions - specifically <a class="tutorial-link" href="https://d3-wiki.readthedocs.io/zh_CN/master/Ordinal-Scales/#ordinal_rangeRoundBands">d3.scale.ordinal(), .domain() and .rangeRoundBands()</a> and the width of the allocated space for the graph. This allows it to evenly allocate each bar it's own space on the graph. In our case, we have 11 data sets as that is how many age ranges there are in each JSON object.
                                                The y-axis is figured out using a similar set of chained d3 functions - d3.scale.linear(), .domain() and .range(). Using these functions (in conjunction with d3.max()), the graph is allocated five points (this number can be figured later) on the y-axis that are numbered evenly from 0 to 5,000. The reason 5,000 is the limit is because, out of the rounded sum of each data_year's murders, 5000 is the highest value.
                                            </p>
                                            <p>
                                                Next, the code actually gets round to defining the axes themselves - beforehand the code was only defining the scale of each axis. The y-axis  is defined first and the number of "ticks" is changed from the default of five to a calculation of height divided by 25. This allows for the graph to be much more readable by displaying a larger amount of ticks when there's enough space for them and still allow it to resize with readability unaffected. The y-axis is also oriented on the "left" - it could be oriented on the right however this would be a very unconventional graph. The x-axis is drawn next and simply is oriented on the bottom. 
                                            </p>
                                        </div>
                                    </div>
                                </div>
                                <!-- End of Section 5.2 -->
                                <!-- Section 5.3 -->
                                <div class="card">
                                    <div class="panel-heading">
                                        <h5 class="panel-title">
                                            <a data-toggle="collapse" data-parent="#accordion" href="#collapse5-3">5.3 Creating the chart</a>
                                        </h5>
                                    </div>
                                    <div id="collapse5-3" class="panel-collapse collapse">
                                        <div class="panel-body">
                                            <p>
                                                With the axes defined, the code then appends them both to the SVG that was appended to the "chart" div earlier. With the axes appended to the SVG, the code can then progress on to displaying the stacked bars. It does this by creating groups for each age range with each group having multiple rectangles for the bars themselves. The bars for the age ranges each have their own respective colour - these colours are chosen from an array with 11 elements; each one being a string in the form of a hex code. Each bar has a consistent value for it's width attribute however the height attribute is a result of a formula being applied to the current value of the data set. This is the reason why a bar representing a larger value will in turn be larger and vice versa.
                                            </p>
                                        </div>
                                    </div>
                                </div>
                                <!-- End of Section 5.3 -->
                                <!-- Section 5.4 -->
                                <div class="card">
                                    <div class="panel-heading">
                                        <h5 class="panel-title">
                                            <a data-toggle="collapse" data-parent="#accordion" href="#collapse5-4">5.4 Tooltip mouse events</a>
                                        </h5>
                                    </div>
                                    <div id="collapse5-4" class="panel-collapse collapse">
                                        <div class="panel-body">
                                            <p>
                                                The tooltip, while not technically created at this point, has it's mouse events covered. It only displays when the mouse moves over one of the bars and will have it's display attribute set to "none" whenever the mouse leaves a bar.
                                            </p>
                                        </div>
                                    </div>
                                </div>
                                <!-- End of Section 5.4 -->
                                <!-- Section 5.5 -->
                                <div class="card">
                                    <div class="panel-heading">
                                        <h5 class="panel-title">
                                            <a data-toggle="collapse" data-parent="#accordion" href="#collapse5-5">5.5 Legend creation</a>
                                        </h5>
                                    </div>
                                    <div id="collapse5-5" class="panel-collapse collapse">
                                        <div class="panel-body">
                                            <p>
                                                The legend for the bar chart is the next item to get created: each legend "item" is assigned the class of "legend". This allows D3 to cycle through each one and assign it an 18x18 rectangle filled with the colour for it's respective age range. It takes a similar approach towards the legend text however it uses an anonymous function that implements a switch statement used to return the correct age range text.
                                            </p>
                                        </div>
                                    </div>
                                </div>
                                <!-- End of Section 5.5 -->
                                <!-- Section 5.6 -->
                                <div class="card">
                                    <div class="panel-heading">
                                        <h5 class="panel-title">
                                            <a data-toggle="collapse" data-parent="#accordion" href="#collapse5-6">5.6 Tooltip creation</a>
                                        </h5>
                                    </div>
                                    <div id="collapse5-6" class="panel-collapse collapse">
                                        <div class="panel-body">
                                            <p>
                                                Although the tooltip mouse events were covered above, the tooltip itself wasn't created. It is actually appended to the SVG but has it's "display" set to none initially. This obviously changes when the mouse moves over a bar but initially it is hidden from view. Next, the tooltip appends a rectangle that is used as a backdrop for the text. This rectangle is styled so that it's a slightly opaque white background. This helps create contrast for the text to be readable. Lastly the text gets appended to the tooltip however no actual text gets inserted - that only happens when the mouse hovers over a bar as discussed above. The text is simply given it's styling at this stage.
                                            </p>
                                        </div>
                                    </div>
                                </div>
                                <!-- End of Section 5.6 -->
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-12">
                <div class="panel panel-default">
                    <div class="card">
                        <div class="panel-heading">
                            <h4 class="panel-title">
                                <a data-toggle="collapse" data-parent="#accordion" href="#collapse6">6. Resizing the bar chart for responsiveness</a>
                            </h4>
                        </div>
                        <div id="collapse6" class="panel-collapse collapse">
                            <div class="panel-body">
                                <p>
                                    Given that the chart has it's height and width defined explicitly using pixels as opposed to using percentages, it will not resize on its own when the screen size is changed. With this in mind, there is code in place to first clear the div containing the graph and then re-create it whenever the screen size is changed. The following code accomplishes this:<br>
                                    <code class="code-snippet">
                                    $(window).resize(function() {<br>
                                    $('#chart').empty();<br>
                                    makeChart();<br>
                                    });<br>
                                    </code>
                                </p>
                                <p>
                                    To break down this code, the first line uses jQuery to detect if the screen size changes and uses an anonymous function to then select the div with the ID of "chart", again using jQuery, and then clear it's contents. Once the chart area is cleared, the function that actually creates the graph - makeChart() - is called to re-create the graph. In Section 5.1 above, remember when creating the chart,  the width of the graph is determined using the current screen size. This means that the graph is always going to be displayed at an appropriate size regardless what screen size is in use.
                                </p>
                                <p>
                                    However, without certain checks implemented within the makeChart() function, the graph can actually become so small that the data cannot be read. Take, for example, a mobile device in portrait mode with a screen width of approximately 640 pixels - the makeChart() function would create a graph that is only 490 pixels wide. This is far too small to be able to make out the graph and take in the information it supplies - it can also result in the x-axis labels overlapping with one another. Instead, there are two if statements within the function that set the width of the graph to 860 if it reaches 860 or below and the height to 300 if it reaches 300 or below. This ensures that the graph will always be legible regardless of the screen size. However it will come at the cost of scrolling but in some cases this is unavoidable.
                                </p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-12">
                <div class="panel panel-default">
                    <div class="card">
                        <div class="panel-heading">
                            <h4 class="panel-title">
                                <a data-toggle="collapse" data-parent="#accordion" href="#collapse7">7. Accessibility</a>
                            </h4>
                        </div>
                        <div id="collapse7" class="panel-collapse collapse">
                            <div class="panel-body">
                                <p>
                                    On the visualisation page, there are two buttons that the user can click - both change the colour scheme of the bars on the graph. The "default colours" button will use 11 colours taken from evenly spaced points on a colour wheel whereas the "colour blind mode" button will set the colours to an initial black followed by two alternating shades of grey. The reason these buttons are in place is for accessibility reasons, specifically with the default colour scheme some users who suffer from colour blindness may have trouble differentiating where one bar ends and another begins. However, with the colour scheme being set to alternating shades of grey, the contrast between bars in clear enough to any form of colour blindness. 
                                </p>
                                <p>
                                    The code used to change the colour scheme of the bars/legend is relatively simple - there is jQuery in place that is used to detect whether either button has been clicked and will change the colour schemes of both the legend and the bars accordingly. The code used to do this can be found below:
                                </p>
                                <br><br>
                                <code class="code-snippet">
                                $('.murders').each(function(i, obj){<br>
                                // colour initial bar black to set it aside from the others<br>
                                if (i == 0)<br>
                                {<br>
                                $(obj).css("fill", "black");<br>
                                }<br>
                                <br>
                                // colour the other bars in alternating shades of grey<br>
                                else<br>
                                {<br>
                                if (i % 2 == 0)<br>
                                {<br>
                                $(obj).css("fill", "#737373");<br>
                                }<br>
                                <br>
                                else<br>
                                {<br>
                                $(obj).css("fill", "#bfbfbf");<br>
                                }<br>
                                }<br>
                                });<br>
                                </code>
                                <p>Because we know each bar for an age group has a class of "murders", we can use jQuery to access each one using the ".murders" selector in combination with the ".each" function. Using an anonymous function with the passed in parameters of "i" and "obj" (the count for each iteration of the "loop" and currently selected class respectively) the code can fill each bar with whatever colour we like. The initial age group bars (we know they're the initial age group as i starts at 0) are set to black using the .css function. After the initial age group is coloured, the next bars are coloured using the same .css function but this time it's used in conjunction with the modulus operator. The modulus operator helps determine if the current iteration is an even number or not as only even numbers will leave a remainder of zero. This is how the bars are coloured in alternate shades of grey: even numbers are one shade and odd numbers are another shade.
                                </p>
                                <p>
                                    Colouring the legend boxes is almost exactly the same except with the following differences:
                                </p>
                                <ul>
                                    <li>The selector is replaced from ".murders" to ".legendColour"</li>
                                    <li>Instead of setting the first element of the array to black, we use the last element - this is due to the legend being displayed in reverse</li>
                                    <li>Likewise the modulus operator will test for "!= 0" instead of "== 0" as this ensures correct colouring for the legend</li>
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-12">
                <div class="panel panel-default">
                    <div class="card">
                        <div class="panel-heading">
                            <h4 class="panel-title">
                                <a data-toggle="collapse" data-parent="#accordion" href="#collapse8">8. Collated list of references</a>
                            </h4>
                        </div>
                        <div id="collapse8" class="panel-collapse collapse">
                            <div class="panel-body">
                                <p>
                                    Below is a collated list of external links used in this tutorial page - they each represent helpful reading for fully understanding how this data visualisation was created.
                                </p>
                                <p>
                                    Links on D3.js:
                                </p>
                                <ul>
                                    <li><a class = "tutorial-link" href="https://github.com/d3/d3/releases">https://github.com/d3/d3/releases</a></li>
                                    <li><a class = "tutorial-link" href="https://github.com/d3/d3/wiki/Gallery">https://github.com/d3/d3/wiki/Gallery</a></li>
                                    <li><a class = "tutorial-link" href="https://d3-wiki.readthedocs.io/zh_CN/master/Ordinal-Scales/#ordinal_rangeRoundBands">https://d3-wiki.readthedocs.io/zh_CN/master/Ordinal-Scales/#ordinal_rangeRoundBands</a></li>
                                </ul>
                                <p>
                                    Links on the FBI crime data explorer API:
                                </p>
                                <ul>
                                    <li><a class = "tutorial-link" href="https://crime-data-explorer.fr.cloud.gov/api">https://crime-data-explorer.fr.cloud.gov/api</a></li>
                                    <li><a class = "tutorial-link" href="https://api.data.gov/signup/">https://api.data.gov/signup/</a></li>
                                    <li><a class = "tutorial-link" href="https://github.com/fbi-cde/crime-data-frontend">https://github.com/fbi-cde/crime-data-frontend</a></li>
                                </ul>
                                <p>
                                    Other links used:
                                </p>
                                <ul>
                                    <li><a class = "tutorial-link" href="https://developer.mozilla.org/en-US/docs/Web/HTTP/CORS">https://developer.mozilla.org/en-US/docs/Web/HTTP/CORS</a></li>
                                    <li><a class = "tutorial-link" href="http://bl.ocks.org/mstanaland/6100713">http://bl.ocks.org/mstanaland/6100713</a></li>
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<!-- You guys need to include EOF so that jquery and bootstrap js works -->
<?php
include("eof.php");
?>