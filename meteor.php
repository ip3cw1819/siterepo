<?php

include("header.php");
?>



<div class="container-fluid">
    <div class="row">
        <div class="col-sm-6">
            <div class="card">
                <h3 class="card-title text-center mx-auto">Meteor Visualisation</h3>
                <div class="input-group mb-3">
                  <label style="margin-left: 10px" class = "input-group-append text-center p-2" for="year">Year: </label>
                  <input type="text" class="form-control" placeholder="2000, 1890, 1997, 2011..." value="" name="year" id="year"> 
                </div>
                
                <div class="input-group mb-3">
                  <label style="margin-left: 10px" class = "input-group-append text-center p-2" for="amount">Amount of Results: </label>
                  <input type="text" class="form-control" placeholder="Range 50-400" value="" name="amount" id="amount"> 
                  <div class="input-group-append">
                    <button class="btn btn-outline-secondary" type="button" id="submit">Submit</button>
                  </div>
                </div>
                <p style = "" class="text-center">**DISCLAMER**: The API doesn't have any data after the year 2013, and will return no results.
                Also, if you search for a year and there is no data recorded, it will return no results. For more information, please visit
                <a style="color: blue !important;" href="https://data.nasa.gov/Space-Science/Meteorite-Landings/gh4g-9sfh/data">Meteor Api Data</a> where it shows you what years have data.</p>
                <p style = "" class="text-center">Furthermore, the API hasn't filled out some of their meteor details meaning, some results may not uncluster
                                                                      due to missing details and lack of results for the year.</p>
                <p class="text-center"><strong>Avoid 1980</strong></p>
                <p style = "color: red !important;" class="error text-center"></p>
            </div>
            
            <div class="card">
                <h3 class="card-title text-center mx-auto">Meteor Mass</h3>
                <canvas id="graph"></canvas>
            </div>
            
        </div><!--End of col-->
        
        <div style ="padding: 0;" class="col-sm-6">
            <div style="padding-right :0px;">
                <div class="float-right" id="map"></div>
            </div>
        </div>
        
     
        
    </div>
</div>


<script src ="/js/meteor.js"></script>
    <!-- Need the following code for clustering Google maps markers-->
<script src="https://developers.google.com/maps/documentation/javascript/examples/markerclusterer/markerclusterer.js"></script>
<script async defer src="https://maps.googleapis.com/maps/api/js?key=AIzaSyDL7b5ov5dHqIZ90VYe9XL_O0GoD9FN0KE&callback=initMap"></script>

</body>
</html>