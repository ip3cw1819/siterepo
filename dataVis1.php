<?php
include("header.php");
//http://bl.ocks.org/mstanaland/6100713
?>

<div class="container-fluid" style="padding-left: 0px;">
    
    <!-- Row 1 (Header) -->
    <div class="row">
        <div class ="col-12" style="padding-right: 0px">
            <div class="page-header" style="padding-left:5px;">
              <h1>Crime data visualisation <small>Number of murder victims in the USA by age range from 1991 - 2017</small></h1>
            </div>
        </div>
    </div>
    
    <!-- Row 2 (Controls & Info) -->
    <div class="row">
        <div class ="col-12" style="padding-right: 0px">
            <div class="button-group" style = "padding-left: 5px; padding-top:10px;">
                <button type="button" class="btn btn-primary" id="defaultColourButton">Default colours</button>
                <button type="button" class="btn btn-primary" id="colourBlindButton">Colour blind mode</button>
                <span id = "tutorialLinking">For information on how this visualisation was created, please see <a class="tutorial-link" href="tutorialCrimeData.php">here</a>.</span>
                <span id = "colourBlindInfo"></span>
            </div>
        </div>
    </div>
    
    <!-- Row 3 (Graph)-->
    <div class="row">
        <div class ="col-12" style="padding-right: 0px">
            <div id="chart"></div>
        </div>
    </div>
    
</div>

<script>

// global variables
var isColourBlind = false;
var colors = [];
var nonColourBlindColours = ["#FF0000", "#FF5E00", "#FFA400", "#FFE000", "#FFFF00", "#00FF47", "#00FFE6", "#00A3FF", "#0017FF", "#6F00FF", "#E400FF"];
var dataSet;


// if user changes resizes the window, clear the chart div and recreate the chart
// if this code is not used then the map will never "re-size" and so it will exceed the page's width
$(window).resize(function() {
    $('#chart').empty();
    makeChart();
});


// When the colour blind mode button is clicked
$('#colourBlindButton').click(function() {
    if (isColourBlind == false)
    {    
        $('#colourBlindInfo').append("Please note that the legend corresponds to the graph in the order shown (i.e. read both from top to bottom).");
    }
    isColourBlind = true;
    
    $('.murders').each(function(i, obj){
        // colour initial bar black to set it aside from the others
        if (i == 0)
        {
            $(obj).css("fill", "black");
        }
        
        // colour the other bars in alternating shades of grey
        else
        {
            if (i % 2 == 0)
            {
                $(obj).css("fill", "#737373");
            }
            
            else
            {
                $(obj).css("fill", "#bfbfbf");
            }
        }
    });

    $('.legendColor').each(function(i, obj) {
        // colour initial legend square black to set it aside from the others
        if (i == 10)
        {
            $(obj).css("fill", "black");
        }
        
        // colour the other legend squares in alternating shades of grey
        else
        {
            if (i % 2 != 0)
            {
                $(obj).css("fill", "#bfbfbf");
            }
            
            else
            {
                $(obj).css("fill", "#737373");
            }
        }
    });
}); // End of colour-blind mode button click handler


// Clicking default colour scheme button
$('#defaultColourButton').click(function() {
    colors = nonColourBlindColours;
    isColourBlind = false;
    
    $('#colourBlindInfo').empty();
    $('.murders').each(function(i, obj) {
    
    $(obj).css("fill", colors[i]);
});

    $('.legendColor').each(function(i, obj) {
        $(obj).css("fill", colors.slice().reverse()[i]);
    });
});// End of default colour scheme button click handler

function makeChart() {
    $.ajax({
        "url": "crime.json",
        "type": "get",
        "dataType": "json",
        "error": function() {
            alert("Data couldn't be loaded.");
        },
        "success": function(data) {
            var margin = {
                top: 20,
                right: 120,
                bottom: 35,
                left: 30
            };
            
            // Setup svg using Bostock's margin convention

            var width = $(window).width() - margin.left - margin.right,
                height = $("body").height() - $("h1").height() - $(".button-group").height() - margin.top - margin.bottom - 18;
                
                // following if statements check the width and height and stop stop the graph from being resized to too small a size
                // if the graph becomes too small then the year/value labels begin to overlap one another
                if (width <= 860)
                {
                    width = 860 - margin.left - margin.right;
                }

                if (height <= 300)
                {
                    height = 300;
                }
                
            var svg = d3.select("#chart")
                .append("svg")
                .attr("width", width + margin.left + margin.right)
                .attr("height", height + margin.top + margin.bottom)
                .append("g")
                .attr("transform", "translate(" + margin.left + "," + margin.top + ")");

            var dataIn = data.results;

            // Following function taken from: https://stackoverflow.com/questions/17684921/sort-json-object-in-javascript
            dataIn.sort(function(a, b) {
                return a.data_year - b.data_year;
            });
            // End of code grab

            var parse = d3.time.format("%Y").parse;


            // Transpose the data into layers
            var dataset = d3.layout.stack()(["unknown", "range_0_9", "range_10_19", "range_20_29", "range_30_39", "range_40_49", "range_50_59", "range_60_69", "range_70_79", "range_80_89", "range_90_99"].map(function(crimeStats) {
                return dataIn.map(function(d) {
                    return {
                        x: parse(d.data_year.toString()),
                        y: +d[crimeStats]
                    };
                });
            }));


            // Set x, y and colors
            var x = d3.scale.ordinal()
                .domain(dataset[0].map(function(d) {
                    return d.x;
                }))
                .rangeRoundBands([10, width - 10], 0.02);

            var y = d3.scale.linear()
                .domain([0, d3.max(dataset, function(d) {
                    return d3.max(d, function(d) {
                        return d.y0 + d.y;
                    });
                })])
                .range([height, 0]);


            // "colors" is an array used to store hex values
            // hex values are used for each piece of data displayed on the bar chart and their "legend" colour code
            // if statement is in place to make sure that when the graph is re-created (screen resize) that the correct colour scheme is used
            if (isColourBlind == false)
            {
                colors = nonColourBlindColours;
            }
            
            else
            {
                colors = ["black", "#bfbfbf", "#737373", "#bfbfbf", "#bfbfbf", "#737373", "#bfbfbf", "#bfbfbf", "#737373", "#bfbfbf", "#737373"];
            }

            // Define and draw axes
            var yAxis = d3.svg.axis()
                .scale(y)
                .orient("left")
                .ticks(height/25)
                .tickSize(-width, 0, 0)
                .tickFormat(function(d) {
                    return d;
                });

            var xAxis = d3.svg.axis()
                .scale(x)
                .orient("bottom")
                .tickFormat(d3.time.format("%Y"));

            svg.append("g")
                .attr("class", "y axis")
                .call(yAxis);

            svg.append("g")
                .attr("class", "x axis")
                .attr("transform", "translate(0," + height + ")")
                .call(xAxis);


            // Create groups for each series, rects for each segment 
            var groups = svg.selectAll("g.murders")
                .data(dataset)
                .enter().append("g")
                .attr("class", "murders")
                .style("fill", function(d, i) {
                    return colors[i];
                });

            var rect = groups.selectAll("rect")
                .data(function(d) {
                    return d;
                })
                .enter()
                .append("rect")
                .attr("x", function(d) {
                    return x(d.x);
                })
                .attr("y", function(d) {
                    return y(d.y0 + d.y);
                })
                .attr("height", function(d) {
                    return y(d.y0) - y(d.y0 + d.y);
                })
                .attr("width", x.rangeBand())
                .on("mouseover", function() {
                    tooltip.style("display", null);
                })
                .on("mouseout", function() {
                    tooltip.style("display", "none");
                })
                .on("mousemove", function(d) {
                    var xPosition = d3.mouse(this)[0] - 15;
                    var yPosition = d3.mouse(this)[1] - 25;
                    tooltip.attr("transform", "translate(" + xPosition + "," + yPosition + ")");
                    tooltip.select("text").text(d.y);
                });


            // Draw legend
            var legend = svg.selectAll(".legend")
                .data(colors)
                .enter().append("g")
                .attr("class", "legend")
                .attr("transform", function(d, i) {
                    return "translate(30," + i * 19 + ")";
                });

            legend.append("rect")
                .attr("x", width - 18)
                .attr("width", 18)
                .attr("height", 18)
                .attr("class", "legendColor")
                .style("fill", function(d, i) {
                    return colors.slice().reverse()[i];
                });

            legend.append("text")
                .attr("x", width + 5)
                .attr("y", 9)
                .attr("dy", ".40em")
                .style("text-anchor", "start")
                .text(function(d, i) {
                    switch (i) {
                        case 0:
                            return "90-99 years old";
                        case 1:
                            return "80-89 years old";
                        case 2:
                            return "70-79 years old";
                        case 3:
                            return "60-69 years old";
                        case 4:
                            return "50-59 years old";
                        case 5:
                            return "40-49 years old";
                        case 6:
                            return "30-39 years old";
                        case 7:
                            return "20-29 years old";
                        case 8:
                            return "10-19 years old";
                        case 9:
                            return "0-9 years old";
                        case 10:
                            return "Unknown age";
                    }
                });


            // Prep the tooltip bits, initial display is hidden
            var tooltip = svg.append("g")
                .attr("id", "tooltip")
                .style("display", "none");

            tooltip.append("rect")
                .attr("width", 30)
                .attr("height", 20)
                .attr("fill", "white")
                .style("opacity", 0.5);

            tooltip.append("text")
                .attr("x", 15)
                .attr("dy", "1.2em")
                .style("text-anchor", "middle")
                .attr("font-size", "12px")
                .attr("font-weight", "bold");
        }
    });
}


makeChart();

</script>

<!-- You guys need to include EOF so that jquery and bootstrap js works -->
<?php
include("eof.php");
?>