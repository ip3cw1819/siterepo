<?php
include("header.php");
?>
<div class="container">
    
    <!-- Row 1 (Header) -->
    <div class="row">
        <div class ="col-12">
            <div class="page-header">
                <h1>VizWiz <small>An overview of what this site is and what it can do for you</small></h1>
            </div>
        </div>
    </div>
    
    <!-- Row 2 (General info) -->
    <div class="row">
        <div class ="col-12">
            <h2>What is VizWiz?</h2>
            <p>VizWiz is a website created by <a class = "tutorial-link" href="authors.php">four students</a> at Glasgow Caledonian University within the BSc (Hons) Computing programme as part of our <a class = "tutorial-link" href="https://www.gcu.ac.uk/study/modules/info/?Module=M3W224781">Integrated Project 3</a> module.</p>
            <p>It is centered around the idea of data visualisation (<a class = "tutorial-link" href="https://www.sas.com/en_gb/insights/big-data/data-visualization.html">defined</a> as the "presentation of data in a pictorial or graphical format"). We take data from several sources online (including the FBI, the United States Geological survey, NASA and more) and have used some newly acquired skills to display this data graphically in order to be useful to you, the visitor.</p>
       
            <h2>What can VizWiz do for me?</h2>
            <p>VizWiz is primarily split into two areas: <b>Functional</b> and <b>Educational</b></p>
        </div>
    </div>
    
    <!-- Row 3 (Functionality/education info) -->
    <div class="row">
        <div class ="col-md-6 col-s-12">
            <h3>Functional</h3>
            <p>VizWiz offers the following functionality for users:</p>
           
            <ul>
                <li><a class = "tutorial-link" href ="weather.php">Weather tracking</a> with capabilities to find out what the weather will be like today and over the next 5 days for a location based on either city or a custom latitude/longitude</li>
                <li><a class = "tutorial-link" href ="earthquake.php">Earthquake mapping</a> with the ability to show recorded earthquakes over a chosen period of time/intensity visualised on top of a Google Map</li>
                <li><a class = "tutorial-link" href ="dataVis1.php">Data visualisation for murder victims in the USA</a> sorted by year and age range</li>
                <li><a class = "tutorial-link" href ="meteor.php">Data visualisation for meteor impacts</a> across the globe with the intensity of each impact for a chosen year</li>
                <li><a class = "tutorial-link" href ="socialMediaActivity.php">Data visualisation for social media activity</a></li>
                <li><a class = "tutorial-link" href ="AQComp.php">Data visualisation for air quality</a> comparisons between two chosen countries</li>
            </ul>
        </div>
    
        <div class ="col-md-6 col-s-12">
            <h3>Educational</h3>
            <p>VizWiz also provides tutorial pages on the following topics:</p>
           
            <ul>
                <li><a class = "tutorial-link" href ="tutorialGeoJSON.php">GEOJSON</a></li>
                <li><a class = "tutorial-link" href ="tutorialEarthquake.php">Earthquake data fundamentals</a></li>
                <li><a class = "tutorial-link" href ="tutorialWeather.php">Weather data fundamentals</a></li>
                <li><a class = "tutorial-link" href ="tutorialJavascript.php">JavaScript techniques</a></li>
                <li><a class = "tutorial-link" href ="tutorialCrimeData.php">How the crime data visualisation was created</a></li>
                <li><a class = "tutorial-link" href ="tutorialMeteor.php">How the meteor visualisation was created</a></li>
                <li><a class = "tutorial-link" href ="tutorialSocialMediaActivity.php">How the social media visualisation was created</a></li>
                <li><a class = "tutorial-link" href ="tutorialAQcomp.php">How the air quality visualisation was created</a></li>
            </ul>
        </div>
    </div>
</div>

<?php
include("eof.php");
?>