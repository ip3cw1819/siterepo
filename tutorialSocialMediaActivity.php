<?php
include("header.php");
//http://bl.ocks.org/mstanaland/6100713
?>
<div class="container-fluid" style="padding-left: 0;">
    <div class="row">
        <div class="col-12">
            <div class=" pt-4 container">
                <h2>CryptoCompare Social Media Tutorial</h2>
                <p>CryptoCurrency is what is known as a "global cryptocurrency market data provider". Through the use of their website and API
                one can get real-time data on the CryptoCurrency market. Through the API we can view trade data, order book data,
                blockchain, historical data and the section we are interested in: Social Data. In this tutorial we're going to use the statistics pertaining to 
                the CryptoCurrency subreddit on the popular social media platform Reddit. The data will be historical data dating back 30 days and will use
                the comment and post data to create a highly visual represenation as seen <a href="socialMediaActivity.php"><font color="blue">here</font></a>.</p>
                <div class="panel-group" id="accordion">
                    <!--Introduces the Concepts within GeoJSON-->
                    <div class="panel panel-default">
                        <div class="pl-4 card">
                            <div class="panel-heading">
                                <h4 class="panel-title">
                                    <a data-toggle="collapse" data-parent="#accordion"
                                       href="#collapse1">1. Connecting to the API</a>
                                </h4>
                            </div>
                            <div id="collapse1" class="panel-collapse collapse in">
                                <div class="pb-2 panel-body"> The very first thing we'll need to do before even thinking about creating the visualisation
                                is acquiring the API key we'll need to access the CryptoCompare API. Thankfully you can create a free account 
                                <a href="https://www.cryptocompare.com/"><font color="blue">here</font></a>
                                and you'll be able to request your own API key in the accounts page.<br>
                                Next we'll need the actual API URL, which you can either acquire in the Social data tab after selecting "use key".<br>
                                Once you're done you should have this URL string:<br> 
                                <font color="red">https://min-api.cryptocompare.com/data/social/coin/histo/day?&api_key=</font><br>
                                Simply append your own API key to the end to gain access to the API data we'll be working with<br>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="panel panel-default">
                        <div class="pl-4 card">
                            <div class="panel-heading">
                                <h4 class="panel-title">
                                    <a data-toggle="collapse" data-parent="#accordion" href="#collapse2">a2</a>
                                </h4>
                            </div>
                            <div id="collapse2" class="panel-collapse collapse in">
                                <div class="pb-2 panel-body">
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="panel panel-default">
                        <div class="panel panel-default">
                            <div class="pl-4 card">
                                <div class="panel-heading">
                                    <h4 class="panel-title">
                                        <a data-toggle="collapse" data-parent="#accordion" href="#collapse4">a3</a>
                                    </h4>
                                </div>
                                <div id="collapse4" class="panel-collapse collapse">
                                    <div class="pb-2 panel-body">
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="panel panel-default">
                            <div class="pl-4 pb-1 card">
                                <div class="panel-heading">
                                    <h4 class="panel-title">
                                        <a data-toggle="collapse" data-parent="#accordion" href="#collapse5">a4</a>
                                    </h4>
                                </div>
                                <div id="collapse5" class="panel-collapse collapse">
                                    <div class="pb-2 panel-body">
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="panel panel-default">
                            <div class="pl-4 pb-1 card">
                                <div class="panel-heading">
                                    <h4 class="panel-title">
                                        <a data-toggle="collapse" data-parent="#accordion" href="#collapse6">a5</a>
                                    </h4>
                                </div>
                                <div id="collapse6" class="panel-collapse collapse">
                                    <div class="pb-2 panel-body">
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- You guys need to include EOF so that jquery and bootstrap js works -->
<?php
include("eof.php");
?>